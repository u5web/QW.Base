﻿using QW.Domain.Module;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace QW.Domain.Repository
{
    /// <summary>
    /// 领域模块数据库选项
    /// </summary>
    public static class DomainModuleDatabaseOption
    {
        /// <summary>
        /// 数据库配置
        /// </summary>
        private static Dictionary<Type, DatabaseConnectionOption> databaseOptions = new Dictionary<Type, DatabaseConnectionOption>();

        /*  构建方式
        /// <summary>
        /// 设置数据库连接选项
        /// </summary>
        /// <param name="option"></param>
        /// <param name="dbOptionsBuilder"></param>
        public static void SetDatabaseOption(this IDomainModuleOption option, Action<DatabaseConnectionOption> dbOptionsBuilder)
        {
            DatabaseConnectionOption dbopt;
            if (databaseOptions.ContainsKey(option.MainObjectType)) { dbopt = databaseOptions[option.MainObjectType]; }
            else { dbopt = new DatabaseConnectionOption(); }

            dbOptionsBuilder?.Invoke(dbopt);

            if (databaseOptions.ContainsKey(option.MainObjectType)) { databaseOptions[option.MainObjectType] = dbopt; }
            else { databaseOptions.Add(option.MainObjectType, dbopt); }
        }
        */

        /// <summary>
        /// 指定数据库连接选项
        /// </summary>
        /// <param name="option"></param>
        /// <param name="dbOption"></param>
        public static void SetDatabaseOption(this IDomainModuleOption option, DatabaseConnectionOption dbOption)
        {
            DatabaseConnectionOption dbopt;
            if (databaseOptions.ContainsKey(option.MainObjectType)) { dbopt = databaseOptions[option.MainObjectType]; }
            else { dbopt = new DatabaseConnectionOption(); }

            dbopt = dbOption;

            if (databaseOptions.ContainsKey(option.MainObjectType)) { databaseOptions[option.MainObjectType] = dbopt; }
            else { databaseOptions.Add(option.MainObjectType, dbopt); }
        }

        /// <summary>
        /// 获取数据库连接选项
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static DatabaseConnectionOption GetDatabaseOption(this IDomainModuleOption option)
        {
            DatabaseConnectionOption result = null;
            if (databaseOptions.ContainsKey(option.MainObjectType))
            {
                result = databaseOptions[option.MainObjectType];
            }
            return result;
        }
    }
}
