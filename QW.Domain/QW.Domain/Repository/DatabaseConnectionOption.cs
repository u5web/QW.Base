﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Domain.Repository
{
    /// <summary>
    /// 数据库连接配置
    /// </summary>
    public class DatabaseConnectionOption
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public DatabaseProvider DBType { get; set; }
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 从库连接字符串集
        /// </summary>
        public List<DatabaseSlaveConnectionOption> SlaveConnectionOptions { get; set; }
    }

    /// <summary>
    /// 从库连接配置
    /// </summary>
    public class DatabaseSlaveConnectionOption
    {
        /// <summary>
        /// 权重
        /// </summary>
        public int Weight = 1;
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
    }

    /// <summary>
    /// 数据库提供者
    /// </summary>
    public enum DatabaseProvider
    {
        Sqlite = 0,
        MySql = 1,
        SqlServer = 2,
        PostgreSQL = 3,
        Oracle = 4,
    }
}
