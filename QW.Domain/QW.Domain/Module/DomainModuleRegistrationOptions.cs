﻿using Microsoft.Extensions.DependencyInjection;
using QW.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Domain.Module
{
    /// <summary>
    /// 领域模块注册选项
    /// </summary>
    public class DomainModuleRegistrationOptions : IDomainModuleOption
    {
        /// <summary>
        /// 服务集合
        /// </summary>
        public IServiceCollection Services { get; set; }

        /// <summary>
        /// 主对象类型
        /// </summary>
        public Type MainObjectType { get; }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="mainObjectType"></param>
        /// <param name="services"></param>
        public DomainModuleRegistrationOptions(Type mainObjectType, IServiceCollection services)
        {
            MainObjectType = mainObjectType;
            Services = services;
        }
    }
}
