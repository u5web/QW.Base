﻿using QW.Domain.Repository;
using System;

namespace QW.Domain.Module
{
    /// <summary>
    /// 领域服务模块
    /// </summary>
    public interface IDomainModule
    {
        /// <summary>
        /// 模块选项
        /// </summary>
        //IDomainModuleOption Option { get; set; }

        /// <summary>
        /// 配置模块时
        /// </summary>
        /// <param name="context"></param>
        void OnConfiguration(DomainModuleConfigurationContext context);

        /// <summary>
        /// 初始模块时
        /// </summary>
        /// <param name="context"></param>
        void OnInitialize(DomainModuleInitializationContext context);

    }
}
