﻿using Microsoft.Extensions.DependencyInjection;
using QW.Domain.Repository;
using System;

namespace QW.Domain.Module
{
    /// <summary>
    /// 领域服务模块
    /// </summary>
    public interface IDomainModuleOption
    {
        /// <summary>
        /// 服务集合
        /// </summary>
        IServiceCollection Services { get; }

        /// <summary>
        /// 主对象类型
        /// </summary>
        Type MainObjectType { get; }
    }
}
