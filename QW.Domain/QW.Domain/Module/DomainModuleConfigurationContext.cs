﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Domain.Module
{
    /// <summary>
    /// 模块配置上下文
    /// </summary>
    public class DomainModuleConfigurationContext
    {
        public IServiceCollection Services { get; private set; }

        public IDomainModuleOption Option { get; private set; }

        /*
        public IDictionary<string, object> Items { get; }

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get
            {
                object result = null;
                if (!Items.TryGetValue(key, out result))
                {
                    result = null;
                }
                return result;
            }
            set
            {
                Items[key] = value;
            }
        }
        */
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="option"></param>
        public DomainModuleConfigurationContext(IServiceCollection services, IDomainModuleOption option)
        {
            Services = services;
            Option = option;
            //Items = new Dictionary<string, object>();
        }
    }
}
