﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Domain.Module
{
    /// <summary>
    /// 模块初始上下文
    /// </summary>
    public class DomainModuleInitializationContext
    {
        /// <summary>
        /// 服务提供者
        /// </summary>
        public IServiceProvider ServiceProvider { get; private set; }
        /// <summary>
        /// 模块选项
        /// </summary>
        public IDomainModuleOption Option { get; private set; }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="option"></param>
        public DomainModuleInitializationContext(IServiceProvider serviceProvider, IDomainModuleOption option)
        {
            ServiceProvider = serviceProvider;
            Option = option;
        }
    }
}
