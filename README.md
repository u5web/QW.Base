# QW.Base

#### 介绍
自己常用系统最基础库，包括基础扩展，MD5,DES,AES加密的辅助工具，缓存，IOC，文件系统，日志，事件中心的基础策略。

#### 支持框架
net45;netstandard2.0;

#### 第三方依赖
log4net 2.0.8,Newtonsoft.Json 12.0.2,System.Reflection.Emit 4.3.0

#### 使用说明
1. 直接生dll后引用
2. 使用内存缓存机制，QW.Core.Cache.CacheHandlerBuilder.Create().UseMemoryCache().Build();
3. 使用本地文件系统，QW.Core.File.FileHandlerBuilder.Create().UseLocalFileHandler().Build();
4. 使用log4net，需要引用log4net，QW.Core.Log.LogHandlerBuilder.Create().Uselog4netHandler("log4net.config").Build();