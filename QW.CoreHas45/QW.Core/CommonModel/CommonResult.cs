﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.CommonModel
{
    /// <summary>
    /// 通用数据返回动态模型
    /// </summary>
    public class CommonResult
    {
        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        public CommonResult()
        {
            //给初始值
            success = false;
            code = -1;
            msg = "";
        }
        #endregion

        #region 属性
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// 信息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 状态
        /// <para>1表成功，0表示未处理（默认）</para>
        /// </summary>
        public int code { get; set; }
        #endregion

        #region 快捷返回
        /// <summary>
        /// 结果
        /// </summary>
        /// <param name="success"></param>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static CommonResult Result(bool success, string msg, int code)
        {
            CommonResult result = new CommonResult
            {
                success = success,
                msg = msg,
                code = code
            };
            return result;
        }
        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static CommonResult SuccessResult(string msg = null, int code = (int)CommonResultCode.success)
        {
            CommonResult result = new CommonResult
            {
                success = true,
                msg = msg,
                code = code
            };
            return result;
        }
        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static CommonResult FailResult(string msg = null, int code = (int)CommonResultCode.fail)
        {
            CommonResult result = new CommonResult
            {
                success = false,
                msg = msg,
                code = code
            };
            return result;
        }
        #endregion
    }
    /// <summary>
    /// 通用数据返回动态模型
    /// </summary>
    public class CommonResult<T> : CommonResult
    {
        #region 属性
        /// <summary>
        /// 数据总量
        /// </summary>
        public long total { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public T data { get; set; }
        #endregion

        #region 快捷返回
        /// <summary>
        /// 结果
        /// </summary>
        /// <param name="success"></param>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <param name="data"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static CommonResult<T> Result(bool success, string msg, int code, T data, int total = 0)
        {
            var result = new CommonResult<T>
            {
                success = success,
                msg = msg,
                code = code,
                data = data,
                total = total
            };
            return result;
        }
        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data"></param>
        /// <param name="total"></param>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static CommonResult<T> SuccessResult(T data, int total = 0, string msg = null, int code = (int)CommonResultCode.success)
        {
            dynamic result = new CommonResult<T>
            {
                success = true,
                msg = msg,
                code = code,
                data = data,
                total = total
            };
            return result;
        }
        #endregion
    }
    /// <summary>
    /// 通用返回状态码
    /// </summary>
    public enum CommonResultCode
    {
        /// <summary>
        /// 失败
        /// </summary>
        fail = -1,
        /// <summary>
        /// 成功
        /// </summary>
        success = 0,
    }
}
