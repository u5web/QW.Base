﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//一些通用的导入结果返回模型
namespace QW.Core.CommonModel
{
    /// <summary>
    /// 通用导入错误记录模型
    /// </summary>
    public class ImportCommonErrorModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 索引条目
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 错误
        /// </summary>
        public string Error { get; set; }
    }


    /// <summary>
    /// 导入通用返回
    /// </summary>
    public class ImportCommonResult
    {
        /// <summary>
        /// 导入成功数量
        /// </summary>
        public int SuccessCount { get; set; }
        /// <summary>
        /// 导入失败数量
        /// </summary>
        public int ErrorCount { get; set; }
    }
}
