﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//一些通用的查询参数模型
namespace QW.Core.CommonModel
{
    /// <summary>
    /// 基础查询实体
    /// </summary>
    public class BaseQuery
    {
        /// <summary>
        /// 页号
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// 每页数量
        /// </summary>
        public int Limit { get; set; } = 10;
        /// <summary>
        /// 是否需要统计总数
        /// </summary>
        public bool NeedTotal { get; set; } = true;
    }


    /// <summary>
    /// 基础查询实体(带排序)
    /// <para>一般适应于前端单字段排序需求</para>
    /// </summary>
    public class BaseSortQuery : BaseQuery
    {
        /// <summary>
        /// 排序字段
        /// </summary>
        public string OrderBy { get; set; }
        /// <summary>
        /// 排序方式
        /// <para>asc 升序,desc 降序</para>
        /// </summary>
        public string Sort { get; set; }
    }
}
