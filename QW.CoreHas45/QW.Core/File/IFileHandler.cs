﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.File
{
    /// <summary>
    /// 文件处理器接口
    /// </summary>
    public interface IFileHandler
    {
        /// <summary>
        /// 构建
        /// </summary>
        void Build();
        #region 文件
        /// <summary>
        /// 获取文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <returns></returns>
        byte[] GetFileContent(string name);
        /// <summary>
        /// 获取文件流
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <returns></returns>
        Stream GetFileStream(string name);
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="stream"></param>
        /// <param name="overwrite">是否覆盖已存在文件</param>
        void CreateFile(string name, Stream stream, bool overwrite = false);
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">文件内容</param>
        /// <param name="overwrite">是否覆盖已存在文件</param>
        /// <param name="encoding">字符编码;默认 utf-8</param>
        void CreateFile(string name, string content, bool overwrite = false, Encoding encoding = null);
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">文件内容</param>
        /// <param name="overwrite">是否覆盖已存在文件</param>
        void CreateFile(string name, byte[] content, bool overwrite = false);

        /// <summary>
        /// 追加文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="stream">文件流</param>
        void AppendFile(string name, Stream stream);

        /// <summary>
        /// 追加文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">追加内容</param>
        /// <param name="encoding">字符编码;默认 utf-8</param>
        void AppendFile(string name, string content, Encoding encoding = null);
        /// <summary>
        /// 追加文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">追加内容</param>
        void AppendFile(string name, byte[] content);
        /// <summary>
        /// 文件是否存在
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="isRelative">是否相对路径;默认 true</param>
        /// <returns></returns>
        bool ExistFile(string name,bool isRelative= true);
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="name">文件名称</param>
        void DeleteFile(string name);

        /// <summary>
        /// 删除多个文件
        /// </summary>
        /// <param name="names">文件名称集合</param>
        void DeleteFiles(List<string> names);
        /// <summary>
        /// 复制文件
        /// </summary>
        /// <param name="source">源文件名称</param>
        /// <param name="target">目标文件名称</param>
        /// <param name="overwrite">是否允许覆盖 默认为false</param>
        void CopyFile(string source, string target, bool overwrite = false);

        /// <summary>
        /// 移动文件
        /// </summary>
        /// <param name="source">源文件名称</param>
        /// <param name="target">目标文件名称</param>
        /// <param name="overwrite">是否允许覆盖 默认为false</param>
        void MoveFile(string source, string target, bool overwrite = false);

        /// <summary>
        /// 获取文件元信息
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <returns></returns>
        FileMetaInfo GetFileMetaInfo(string name);
        #endregion

        #region 目录
        /// <summary>
        /// 创建目录
        /// </summary>
        /// <param name="name">目录名称</param>
        void CreateFolder(string name);
        /// <summary>
        /// 目录是否存在
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <param name="isRelative">是否相对路径;默认 true</param>
        /// <returns></returns>
        bool ExistFolder(string name, bool isRelative = true);
        /// <summary>
        /// 删除目录
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <param name="recursive">是否移除子目录和文件;默认 false</param>
        void DeleteFolder(string name, bool recursive = false);
        /// <summary>
        /// 获取目录下的文件
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <param name="isFullName">是否全路径名</param>
        /// <returns></returns>
        List<string> GetFolderFiles(string name,bool isFullName=false);
        /// <summary>
        /// 获取目录下的子目录
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <param name="self">是否包含自身</param>
        /// <param name="isFullName">是否全路径名</param>
        /// <returns></returns>
        List<string> GetFolderSubFolder(string name, bool self = false, bool isFullName = false);
        /// <summary>
        /// 获取目录元信息
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <returns></returns>
        FolderMetaInfo GetFolderMetaInfo(string name);
        /// <summary>
        /// 复制目录
        /// </summary>
        /// <param name="source">源文件夹</param>
        /// <param name="target">目标文件夹</param>
        /// <param name="includeFile">是否复制文件</param>
        /// <returns></returns>
        bool CopyFolder(string source, string target, bool includeFile);
        /// <summary>
        /// 移动目录
        /// </summary>
        /// <param name="source">源文件夹</param>
        /// <param name="target">目标文件夹</param>
        /// <returns></returns>
        bool MoveFolder(string source, string target);
        #endregion

        /// <summary>
        /// 获取远程全路径地址
        /// </summary>
        /// <returns></returns>
        string GetRomotePath(string path);
    }
}
