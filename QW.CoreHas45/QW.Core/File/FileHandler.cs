﻿using QW.Core.Helper;
using QW.Core.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.File
{
    /// <summary>
    /// 文件处理器
    /// </summary>
    public class FileHandler
    {
        private static IFileHandler _handle;
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="f"></param>
        internal static void Register(IFileHandler f)
        {
            _handle = f;
        }
        #region 文件
        /// <summary>
        /// 获取文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <returns></returns>
        public static byte[] GetFileContent(string name)
        {
            return _handle.GetFileContent(name);
        }
        /// <summary>
        /// 获取文件流
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <returns></returns>
        public static Stream GetFileStream(string name)
        {
            return _handle.GetFileStream(name);
        }
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="stream"></param>
        /// <param name="overwrite">是否覆盖已存在文件</param>
        public static void CreateFile(string name, Stream stream, bool overwrite = false)
        {
            _handle.CreateFile(name, stream, overwrite);
        }
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">文件内容</param>
        /// <param name="overwrite">是否覆盖已存在文件</param>
        /// <param name="encoding">字符编码;默认 utf-8</param>
        public static void CreateFile(string name, string content, bool overwrite = false, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            _handle.CreateFile(name, content, overwrite, encoding);
        }
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">文件内容</param>
        /// <param name="overwrite">是否覆盖已存在文件</param>
        public static void CreateFile(string name, byte[] content, bool overwrite = false)
        {
            _handle.CreateFile(name, content, overwrite);
        }

        /// <summary>
        /// 追加文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="stream">文件流</param>
        public static void AppendFile(string name, Stream stream)
        {
            _handle.AppendFile(name, stream);
        }

        /// <summary>
        /// 追加文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">追加内容</param>
        /// <param name="encoding">字符编码;默认 utf-8</param>
        public static void AppendFile(string name, string content, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            _handle.AppendFile(name, content);
        }
        /// <summary>
        /// 追加文件内容
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="content">追加内容</param>
        public static void AppendFile(string name, byte[] content)
        {
            _handle.AppendFile(name, content);
        }
        /// <summary>
        /// 文件是否存在
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <param name="isRelative">是否相对路径;默认 true</param>
        /// <returns></returns>
        public static bool ExistFile(string name, bool isRelative = true)
        {
            return _handle.ExistFile(name,isRelative);
        }
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="name">文件名称</param>
        public static void DeleteFile(string name)
        {
            _handle.DeleteFile(name);
        }

        /// <summary>
        /// 删除多个文件
        /// </summary>
        /// <param name="names">文件名称集合</param>
        public static void DeleteFiles(List<string> names)
        {
            _handle.DeleteFiles(names);
        }
        /// <summary>
        /// 复制文件
        /// </summary>
        /// <param name="source">源文件名称</param>
        /// <param name="target">目标文件名称</param>
        /// <param name="overwrite">是否允许覆盖 默认为false</param>
        public static void CopyFile(string source, string target, bool overwrite = false)
        {
            _handle.CopyFile(source, target, overwrite);
        }
        /// <summary>
        /// 移动文件
        /// </summary>
        /// <param name="source">源文件名称</param>
        /// <param name="target">目标文件名称</param>
        /// <param name="overwrite">是否允许覆盖 默认为false</param>
        public static void MoveFile(string source, string target, bool overwrite = false)
        {
            _handle.MoveFile(source, target, overwrite);
        }

        /// <summary>
        /// 获取文件元信息
        /// </summary>
        /// <param name="name">文件名称</param>
        /// <returns></returns>
        public static FileMetaInfo GetFileMetaInfo(string name)
        {
            return _handle.GetFileMetaInfo(name);
        }
        #endregion

        #region 目录
        /// <summary>
        /// 创建目录
        /// </summary>
        /// <param name="name">目录名称</param>
        public static void CreateFolder(string name)
        {
            _handle.CreateFolder(name);
        }
        /// <summary>
        /// 目录是否存在
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <param name="isRelative">是否相对路径;默认 true</param>
        /// <returns></returns>
        public static bool ExistFolder(string name, bool isRelative = true)
        {
            return _handle.ExistFolder(name, isRelative);
        }
        /// <summary>
        /// 删除目录
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <param name="recursive">是否移除子目录和文件；默认 false</param>
        public static void DeleteFolder(string name, bool recursive = false)
        {
            _handle.DeleteFolder(name, recursive);
        }
        /// <summary>
        /// 获取目录下的文件
        /// </summary>
        /// <param name="name">目录名</param>
        /// <param name="isFullName">是否全路径名</param>
        /// <returns></returns>
        public static List<string> GetFolderFiles(string name, bool isFullName = false)
        {
            return _handle.GetFolderFiles(name, isFullName);
        }
        /// <summary>
        /// 获取目录下的子目录
        /// </summary>
        /// <param name="name">目录名</param>
        /// <param name="self">是否包含自身</param>
        /// <param name="isFullName">是否全路径名</param>
        /// <returns></returns>
        public static List<string> GetFolderSubFolder(string name, bool self = false, bool isFullName = false)
        {
            return _handle.GetFolderSubFolder(name, self, isFullName);
        }
        /// <summary>
        /// 获取目录元信息
        /// </summary>
        /// <param name="name">目录名称</param>
        /// <returns></returns>
        public static FolderMetaInfo GetFolderMetaInfo(string name)
        {
            return _handle.GetFolderMetaInfo(name);
        }
        /// <summary>
        /// 复制目录
        /// </summary>
        /// <param name="source">源文件夹</param>
        /// <param name="target">目标文件夹</param>
        /// <param name="includeFile">是否复制文件</param>
        /// <returns></returns>
        public static bool CopyFolder(string source, string target, bool includeFile)
        {
            return _handle.CopyFolder(source, target, includeFile);
        }
        /// <summary>
        /// 移动目录
        /// </summary>
        /// <param name="source">源文件夹</param>
        /// <param name="target">目标文件夹</param>
        /// <returns></returns>
        public static bool MoveFolder(string source, string target)
        {
            return _handle.MoveFolder(source, target);
        }
        #endregion
        /// <summary>
        /// 获取远程全路径地址
        /// </summary>
        /// <returns></returns>
        public static string GetRomotePath(string path)
        {
            return _handle.GetRomotePath(path);
        }
    }
}
