﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.File.Local
{
    /// <summary>
    /// 本地文件处理程序构建器
    /// </summary>
    public static class LocalFileHandlerBuilder
    {
        /// <summary>
        /// 使用本地文件处理程序
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static FileHandlerBuilder UseLocalFileHandler(this FileHandlerBuilder builder)
        {
            builder.RegisterHandler(new LocalFileHandler());
            return builder;
        }
    }
}
