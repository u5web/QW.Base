﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Domain
{
    /// <summary>
    /// 自启动服务约束
    /// </summary>
    public interface IStartupService
    {
        /// <summary>
        /// 预启动
        /// </summary>
        void PreStartup();
        /// <summary>
        /// 启动
        /// </summary>
        void Startup();
    }
}
