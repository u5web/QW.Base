﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.LinqExtensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class IQueryableEx
    {
        /// <summary>
        /// 构建初始查询条件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="defultPredicate">与条件true，或条件false</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> GetDefaultPredicate<T>(this IQueryable<T> source, bool defultPredicate)
        {
            if (defultPredicate)
            {
                return PredicateExtensions.True<T>();
            }
            else
            {
                return PredicateExtensions.False<T>();
            }
        }
        /// <summary>
        /// 获取默认排序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        /// <example>var ord=GetDefaultOrder(d=>d.OrderBy(o=>o.Id))</example>
        public static Func<IQueryable<T>, IOrderedQueryable<T>> GetOrderBy<T>(this IQueryable<T> source, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            if (orderBy == null) throw new ArgumentNullException("初始排序不可为空");
            return orderBy;
        }

    }
}
