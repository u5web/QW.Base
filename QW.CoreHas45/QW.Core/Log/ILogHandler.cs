﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Log
{
    /// <summary>
    /// 日志处理程序接口
    /// </summary>
    public interface ILogHandler
    {
        /// <summary>
        /// 构建
        /// </summary>
        void Build();
        /// <summary>
        /// 设置配置文件
        /// </summary>
        /// <param name="configFile"></param>
        void SetConfig(string configFile);
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        void Error(object message);
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        void Error(object message, Exception ex);
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message">消息</param>
        void Info(object message);
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        void Info(object message, Exception ex);
        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="message">消息</param>
        void Warn(object message);
        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        void Warn(object message, Exception ex);
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message">消息</param>
        void Debug(object message);
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        void Debug(object message, Exception ex);
    }
}
