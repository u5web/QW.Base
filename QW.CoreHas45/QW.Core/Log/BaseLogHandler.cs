﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Log
{
    /// <summary>
    /// 日志处理程序基类
    /// </summary>
    public abstract class BaseLogHandler : ILogHandler
    {
        /// <summary>
        /// 配置文件
        /// </summary>
        protected string ConfigFileName;
        /// <summary>
        /// 构建
        /// </summary>
        public abstract void Build();
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message">消息</param>
        public abstract void Debug(object message);
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public abstract void Debug(object message, Exception ex);
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        public abstract void Error(object message);
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public abstract void Error(object message, Exception ex);
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message">消息</param>
        public abstract void Info(object message);
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public abstract void Info(object message, Exception ex);
        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="message">消息</param>
        public abstract void Warn(object message);
        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public abstract void Warn(object message, Exception ex);
        /// <summary>
        /// 设置配置文件
        /// </summary>
        /// <param name="configFile"></param>
        public virtual void SetConfig(string configFile)
        {
            ConfigFileName = configFile;
        }
        /// <summary>
        /// 默认深度
        /// </summary>
        protected static int DefaultDepth = 2;
        /// <summary>
        /// 获取当前调用方法名
        /// </summary>
        /// <param name="depth">深度</param>
        /// <returns></returns>
        protected static string GetCurrentMethodFullName(int? depth = null)
        {
            try
            {
                int _depth = DefaultDepth;
                if (depth.HasValue)
                {
                    _depth = depth.Value;
                }
                StackTrace st = new StackTrace();
                int maxFrames = st.GetFrames().Length;
                StackFrame sf;
                string methodName, className;
                Type classType;
                do
                {
                    sf = st.GetFrame(_depth++);
                    classType = sf.GetMethod().DeclaringType;
                    className = classType.ToString();
                } while (className.EndsWith("Exception") && depth < maxFrames);
                methodName = sf.GetMethod().Name;
                return className + "." + methodName;
            }
            catch (Exception e)
            {
                var err = e.Message;
                //获取名称失败
                return " QW.Core.Log.BaseLogHandler.GetCurrentMethodFullName";
            }
        }
    }
}
