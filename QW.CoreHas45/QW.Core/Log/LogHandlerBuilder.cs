﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Log
{
    /// <summary>
    /// 日志处理程序构建器
    /// </summary>
    public class LogHandlerBuilder
    {
        private static ILogHandler _handler;
        private string configFile;
        /// <summary>
        /// 构建器
        /// </summary>
        /// <returns></returns>
        public static LogHandlerBuilder Create()
        {
            return new LogHandlerBuilder();
        }
        /// <summary>
        /// 设置配置文件
        /// </summary>
        /// <param name="fileName"></param>
        public LogHandlerBuilder SetConfig(string fileName)
        {
            configFile = fileName;
            return this;
        }
        /// <summary>
        /// 注册处理程序
        /// </summary>
        /// <param name="handle"></param>
        public LogHandlerBuilder RegisterHandler(ILogHandler handle)
        {
            _handler = handle;
            return this;
        }
        /// <summary>
        /// 容器中心构建
        /// </summary>
        public void Build()
        {
            _handler.SetConfig(configFile);
            _handler.Build();
            LogHandler.Register(_handler);
        }
    }
}
