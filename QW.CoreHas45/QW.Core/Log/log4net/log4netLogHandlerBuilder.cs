﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Log.log4net
{
    /// <summary>
    /// log4net处理程序构建器
    /// </summary>
    public static class log4netLogHandlerBuilder
    {
        /// <summary>
        /// 使用log4net
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configFile">配置文件</param>
        /// <param name="repository_Name">仓储名称</param>
        /// <returns></returns>
        public static LogHandlerBuilder Uselog4netHandler(this LogHandlerBuilder builder,string configFile= "log4net.config",string repository_Name= "qw_log_repository")
        {
            var handle = new log4netLogHandler();
            handle.SetRepositoryName(repository_Name);
            builder.SetConfig(configFile);
            builder.RegisterHandler(handle);
            return builder;
        }
    }
}
