﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using log4net.Config;
using log4net.Repository;
using QW.Core.Helper;

namespace QW.Core.Log.log4net
{
    /// <summary>
    /// log4net处理程序
    /// </summary>
    public class log4netLogHandler : BaseLogHandler
    {
        private static string Default_Repository_Name = "qw_log_repository";
        private static ILoggerRepository _store = null;
        private ILoggerRepository GetRepository(string name)
        {
            if (_store == null)
            {
                _store = LogManager.CreateRepository(name);
                if (string.IsNullOrWhiteSpace(ConfigFileName))
                {
                    XmlConfigurator.Configure(_store);
                }
                else
                {
                    XmlConfigurator.Configure(_store, new FileInfo(ConfigFileName));
                }
            }
            return _store;
        }
        /// <summary>
        /// 
        /// </summary>
        public override void Build()
        {
            var _r = GetRepository(Default_Repository_Name);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public override void Debug(object message)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Debug(message);
            }
            catch
            {

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public override void Debug(object message, Exception ex)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Debug(message, ex);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public override void Error(object message)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Error(message);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public override void Error(object message, Exception ex)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Error(message, ex);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public override void Info(object message)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Info(message);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public override void Info(object message, Exception ex)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Info(message, ex);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public override void Warn(object message)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Warn(message);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public override void Warn(object message, Exception ex)
        {
            try
            {
                var _r = GetRepository(Default_Repository_Name);
                ILog log = LogManager.GetLogger(_r.Name, GetCurrentMethodFullName());
                log.Warn(message, ex);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public void SetRepositoryName(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                Default_Repository_Name = name;
            }
        }
    }
}
