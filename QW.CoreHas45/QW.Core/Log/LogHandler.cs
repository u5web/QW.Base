﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Log
{
    /// <summary>
    /// 日志处理程序
    /// </summary>
    public class LogHandler
    {
        private static ILogHandler _handle;
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="f"></param>
        internal static void Register(ILogHandler f)
        {
            _handle = f;
        }
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        public static void Error(object message)
        {
            if (_handle == null) { return; }
            _handle.Error(message);
        }
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public static void Error(object message, Exception ex)
        {
            if (_handle == null) { return; }
            _handle.Error(message, ex);
        }
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message">消息</param>
        public static void Info(object message)
        {
            if (_handle == null) { return; }
            _handle.Info(message);
        }
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public static void Info(object message, Exception ex)
        {
            if (_handle == null) { return; }
            _handle.Info(message, ex);
        }
        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="message">消息</param>
        public static void Warn(object message)
        {
            if (_handle == null) { return; }
            _handle.Warn(message);
        }
        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public static void Warn(object message, Exception ex)
        {
            if (_handle == null) { return; }
            _handle.Warn(message, ex);
        }
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message">消息</param>
        public static void Debug(object message)
        {
            if (_handle == null) { return; }
            _handle.Debug(message);
        }
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ex">异常</param>
        public static void Debug(object message, Exception ex)
        {
            if (_handle == null) { return; }
            _handle.Debug(message, ex);
        }
    }
}
