﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 生命周期注入约束
    /// </summary>
    public interface IScopedDependency : IDependency
    {
    }
}
