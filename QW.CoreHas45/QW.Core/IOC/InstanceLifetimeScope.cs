﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 实例生命周期
    /// </summary>
    public enum InstanceLifetimeScope
    {
        /// <summary>
        /// 瞬态
        /// </summary>
        Transient = 0,
        /// <summary>
        /// 作用域
        /// </summary>
        Scoped = 1,
        /// <summary>
        /// 单例
        /// </summary>
        Singleton = 100
    }
}
