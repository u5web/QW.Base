﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 控制反转容器
    /// </summary>
    public class IOCContainer
    {
        /// <summary>
        /// 控制反转容器
        /// </summary>
        private static IIOCContainer _center = null;
        /// <summary>
        /// 注册控制反转容器
        /// </summary>
        /// <param name="c"></param>
        internal static void Register(IIOCContainer c)
        {
            _center = c;
        }
        /// <summary>
        /// 反转实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            CheckInitialized();
            return _center.Resolve<T>();
        }
        /// <summary>
        /// 反转实例
        /// </summary>
        /// <param name="T"></param>
        /// <returns></returns>
        public static object Resolve(Type T)
        {
            //TODO:DZY[200502]反转多个的情况不考虑
            CheckInitialized();
            return _center.Resolve(T);
        }
        /// <summary>
        /// 检测是否已注册容器
        /// </summary>
        private static void CheckInitialized()
        {
            if (_center == null) { throw new Exceptions.InstanceCreateException("请先注册容器"); }
        }
    }
}
