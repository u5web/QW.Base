﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 控制反转容器构建器
    /// </summary>
    public class IOCContainerBuilder
    {
        /// <summary>
        /// 容器
        /// </summary>
        private IIOCContainer _container;
        /// <summary>
        /// 注册类型集
        /// </summary>
        private List<Tuple<Type, Type, InstanceLifetimeScope>> registerTypes = new List<Tuple<Type, Type, InstanceLifetimeScope>>();
        /// <summary>
        /// 构建器
        /// </summary>
        /// <returns></returns>
        public static IOCContainerBuilder Create()
        {
            return new IOCContainerBuilder();
        }
        /// <summary>
        /// 注册容器
        /// </summary>
        /// <param name="c"></param>
        public IOCContainerBuilder RegisterContainer(IIOCContainer c)
        {
            _container = c;
            return this;
        }
        /// <summary>
        /// 容器中心构建
        /// </summary>
        public void Build()
        {
            //注册类型
            foreach (var item in registerTypes)
            {
                _container.RegisterType(item.Item1, item.Item2, item.Item3);
            }
            //执行构建
            _container.Build();
            IOCContainer.Register(_container);
        }
        /// <summary>
        /// 注册类型
        /// </summary>
        /// <typeparam name="TT">目标类型，例：接口</typeparam>
        /// <typeparam name="T">实例源类型，例：接口实现</typeparam>
        /// <param name="lifetimeScope">生命周期</param>
        public IOCContainerBuilder RegisterType<TT, T>(InstanceLifetimeScope lifetimeScope = InstanceLifetimeScope.Transient)
        {
            registerTypes.Add(new Tuple<Type, Type, InstanceLifetimeScope>(typeof(TT), typeof(T), lifetimeScope));
            return this;
        }
        /// <summary>
        /// 注册类型
        /// </summary>
        /// <param name="TT">目标类型，例：接口</param>
        /// <param name="T">实例源类型，例：接口实现</param>
        /// <param name="lifetimeScope">生命周期</param>
        public IOCContainerBuilder RegisterType(Type TT, Type T, InstanceLifetimeScope lifetimeScope = InstanceLifetimeScope.Transient)
        {
            registerTypes.Add(new Tuple<Type, Type, InstanceLifetimeScope>(TT, T, lifetimeScope));
            return this;
        }
        /// <summary>
        /// 注册类型
        /// </summary>
        /// <typeparam name="TT">目标类型，例：接口</typeparam>
        /// <param name="T">实例源类型，例：接口实现</param>
        /// <param name="lifetimeScope">生命周期</param>
        public IOCContainerBuilder RegisterType<TT>(Type T, InstanceLifetimeScope lifetimeScope = InstanceLifetimeScope.Transient)
        {
            registerTypes.Add(new Tuple<Type, Type, InstanceLifetimeScope>(typeof(TT), T, lifetimeScope));
            return this;
        }
        /// <summary>
        /// 注册实例
        /// <para>将实例单例注入,需要注入容器后才可以使用</para>
        /// </summary>
        /// <typeparam name="T">实例源类型</typeparam>
        /// <param name="instance">实例</param>
        public IOCContainerBuilder RegisterInstance<T>(T instance) where T : class
        {
            if (_container == null)
            {
                throw new Exception("请选注入容器");
            }
            //registerInstances.Add(new Tuple<Type, object>(typeof(T), instance));
            _container.RegisterInstance(instance);
            return this;
        }
    }
}
