﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 控制反转容器接口
    /// </summary>
    public interface IIOCContainer
    {
        /// <summary>
        /// 构建
        /// </summary>
        void Build();
        /// <summary>
        /// 注册类型
        /// </summary>
        /// <param name="TT">目标类型，例：接口</param>
        /// <param name="T">实例源类型，例：接口实现</param>
        /// <param name="lifetimeScope">生命周期</param>
        void RegisterType(Type TT, Type T, InstanceLifetimeScope lifetimeScope = InstanceLifetimeScope.Transient);
        /// <summary>
        /// 注册类型
        /// </summary>
        /// <typeparam name="TT">目标类型，例：接口</typeparam>
        /// <typeparam name="T">实例源类型，例：接口实现</typeparam>
        /// <param name="lifetimeScope">生命周期</param>
        void RegisterType<TT, T>(InstanceLifetimeScope lifetimeScope = InstanceLifetimeScope.Transient);
        /// <summary>
        /// 注册类型
        /// </summary>
        /// <typeparam name="TT">目标类型，例：接口</typeparam>
        /// <param name="T">实例源类型，例：接口实现</param>
        /// <param name="lifetimeScope">生命周期</param>
        void RegisterType<TT>(Type T, InstanceLifetimeScope lifetimeScope = InstanceLifetimeScope.Transient);
        /// <summary>
        /// 注册实例
        /// <para>将实例单例注入</para>
        /// </summary>
        /// <typeparam name="T">实例源类型</typeparam>
        /// <param name="instance">实例</param>
        void RegisterInstance<T>(T instance) where T : class;
        /// <summary>
        /// 反转实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Resolve<T>();
        /// <summary>
        /// 反转实例
        /// </summary>
        /// <param name="T"></param>
        /// <returns></returns>
        object Resolve(Type T);
    }
}
