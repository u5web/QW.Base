﻿using QW.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QW.Core.IOC.Dictionary
{
    /// <summary>
    /// 字典容器构建器
    /// </summary>
    public static class DictionaryIOCContainerBuiler
    {
        /// <summary>
        /// 使用默认容器(字典)
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IOCContainerBuilder UseDictionaryIOCContainer(this IOCContainerBuilder builder)
        {
            builder.RegisterContainer(new DictionaryIOCContainer());
            return builder;
        }
    }
}
