﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 瞬态注入约束
    /// </summary>
    public interface ITransientDependency : IDependency
    {
    }
}
