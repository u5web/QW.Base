﻿using QW.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// IOC异常
    /// </summary>
    public class IOCException : QWBaseException
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public IOCException() { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>

        public IOCException(string message) : base(message) { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>

        public IOCException(string message, Exception inner) : base(message, inner) { }
    }
}
