﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件执行序号
    /// <para>序号只影响加载顺序，不确保执行顺序</para>
    /// </summary>
    public class EventOrdinalAttribute : Attribute
    {
        /// <summary>
        /// 排序号
        /// </summary>
        public int Ordinal { get; set; }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="ordinal"></param>
        public EventOrdinalAttribute(int ordinal)
        {
            this.Ordinal = ordinal;
        }
    }
}
