﻿namespace QW.Core.Events.Default
{
    /// <summary>
    /// 默认事件总线构建器
    /// </summary>
    public static class DefaultEventBusBuilder
    {
        /// <summary>
        /// 使用默认事件总线
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        public static EventBusBuilder UseDefaultEventBus(this EventBusBuilder builder, IEventStore store)
        {
            builder.RegisterBus(new DefaultEventBus(store));
            return builder;
        }
    }
}
