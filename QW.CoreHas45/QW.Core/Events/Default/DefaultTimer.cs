﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QW.Core.Events.Default
{
    /// <summary>
    /// 默认定时器
    /// </summary>
    internal class DefaultTimer
    {
        private Action action;
        private Timer timer;
        public DefaultTimer(Action action, int interval)
        {
            this.action = action;
            this.timer = new Timer(Tick, new DefaultTimerState(), 0, interval);
        }


        private void Tick(object obj)
        {
            var state = obj as DefaultTimerState;
            if (state.IsRun) return;
            state.IsRun = true;
            try
            {
                action();
            }
            finally
            {
                state.IsRun = false;
            }
        }
    }
    internal class DefaultTimerState
    {
        public bool IsRun { get; set; }
    }
}
