﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件异常
    /// </summary>
    public class EventException : Exception
    {
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        public EventException(string message) : base(message)
        {
        }
    }
}
