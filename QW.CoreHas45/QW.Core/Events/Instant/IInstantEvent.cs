﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 瞬时事件声明
    /// </summary>
    public interface IInstantEvent : IEvent
    {
    }
    /// <summary>
    /// 瞬时事件声明
    /// </summary>
    /// <typeparam name="TSender"></typeparam>
    public interface IInstantEvent<TSender> : IInstantEvent
    {

        /// <summary>
        /// 触发事件源
        /// </summary>
        TSender Sender { get; }
    }
}
