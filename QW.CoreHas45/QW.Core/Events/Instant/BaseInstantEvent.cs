﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件基础模型
    /// </summary>
    public abstract class BaseInstantEvent : IInstantEvent
    {
        /// <summary>
        /// 事件唯一标识
        /// </summary>
        [JsonIgnore]
        public Guid Identity { get; }
        /// <summary>
        /// 事件触发时间
        /// </summary>
        [JsonIgnore]
        public DateTime EventTime { get; }
        /// <summary>
        /// .ctor
        /// </summary>
        public BaseInstantEvent()
        {
            this.Identity = Guid.NewGuid();
            this.EventTime = DateTime.Now;
        }
    }
    /// <summary>
    /// 事件基础模型
    /// </summary>
    public abstract class BaseInstantEvent<TSender> : BaseInstantEvent, IInstantEvent<TSender>
    {
        /// <summary>
        /// 事件触发源对象
        /// </summary>
        public TSender Sender { get; }
        /// <summary>
        /// .ctor
        /// </summary>
        public BaseInstantEvent(TSender sender)
        {
            this.Sender = sender;
        }
    }
}
