﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件比对
    /// </summary>
    public class EventComparer : EqualityComparer<EventData>
    {
        /// <summary>
        /// 是否相等
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public override bool Equals(EventData x, EventData y)
        {
            if (x.EventId == y.EventId)
                return true;
            return false;
        }
        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override int GetHashCode(EventData obj)
        {
            return obj.EventId.GetHashCode();
        }
    }
}
