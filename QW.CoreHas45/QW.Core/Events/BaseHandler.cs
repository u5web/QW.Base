﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 处理器基类
    /// </summary>
    public class BaseHandler : IHandler
    {
        /// <summary>
        /// 发布瞬时事务
        /// </summary>
        /// <param name="e"></param>
        protected static void Publish(BaseInstantEvent e)
        {
            EventBus.Publish(e);
        }
        /// <summary>
        /// 发布延时事务
        /// </summary>
        /// <param name="e"></param>
        protected static void Publish(BaseDelayEvent e)
        {
            EventBus.Publish(e);
        }
    }
}
