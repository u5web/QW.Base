﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件声明接口
    /// </summary>
    public interface IEvent
    {
        /// <summary>
        /// 事件唯一标识
        /// </summary>
        Guid Identity { get; }
        /// <summary>
        /// 事件触发时间
        /// </summary>
        DateTime EventTime { get; }
    }

    /// <summary>
    /// 事件声明接口
    /// </summary>
    public interface IEvent<TSender> : IEvent
    {
        /// <summary>
        /// 事件触发源对象
        /// </summary>
        TSender Sender{ get; }
    }

    
}
