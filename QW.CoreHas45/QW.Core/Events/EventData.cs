﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件数据
    /// </summary>
    public class EventData
    {
        /// <summary>
        /// 事件编号
        /// </summary>
        public Guid EventId { get; set; }
        /// <summary>
        /// 事件时间
        /// </summary>
        public DateTime EventTime { get; set; }
        /// <summary>
        /// 触发时间
        /// </summary>
        public DateTime TriggerTime { get; set; }
        /// <summary>
        /// 事件名称
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 事件数据
        /// </summary>
        public string Data { get; set; }
    }
}
