﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件订阅者
    /// </summary>
    public interface IEventSubscriber : IDisposable
    {
        /// <summary>
        /// 订阅事件
        /// </summary>
        /// <param name="handler"></param>
        void Subscribe(EventDetail handler);
        //where TEvent : IEvent
        //where TEventHandler : IEventHandler<TEvent>;
    }
}
