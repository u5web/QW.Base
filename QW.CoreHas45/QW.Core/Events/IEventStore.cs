﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 通用事件仓储
    /// </summary>
    public interface IEventStore
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="events"></param>
        void Save(List<EventData> events);
        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="identitys"></param>
        void Remove(List<Guid> identitys);
        /// <summary>
        /// 拉取
        /// </summary>
        /// <param name="triggerEnd">触发时间</param>
        /// <param name="max">数量</param>
        /// <returns></returns>
        List<EventData> Pull(DateTime triggerEnd, int max);
    }
}
