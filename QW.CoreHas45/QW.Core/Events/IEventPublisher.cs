﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件发布者
    /// </summary>
    public interface IEventPublisher : IDisposable
    {
        /// <summary>
        /// 同步事件同步
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="event"></param>
        void Publish<TEvent>(TEvent @event) where TEvent : class, IEvent;
    }
}
