﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件详情
    /// </summary>
    public class EventDetail
    {
        /// <summary>
        /// 事件名称
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 事件类型信息
        /// </summary>
        public Type TypeInfo { get; set; }
        /// <summary>
        /// 是否延时事务
        /// </summary>
        public bool IsDelay { get; set; }
        /// <summary>
        /// 处理程序集
        /// </summary>
        public List<HandlerDetail> Handlers { get; set; }

    }
    /// <summary>
    /// 处理程序详情
    /// </summary>
    public class HandlerDetail
    {
        /// <summary>
        /// 排序号
        /// </summary>
        public int Ordinal { get; set; }
        /// <summary>
        /// 处理程序
        /// </summary>
        public FastInvoke.FastInvokeHandler Handler { get; set; }
    }
}
