﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QW.Core.Events
{
    /// <summary>
    /// 事件总线
    /// </summary>
    public interface IEventBus :IEventPublisher,IEventSubscriber
    {
        /// <summary>
        /// 获取注册中的事件集
        /// </summary>
        /// <param name="name">事件名称,默认空 表示取所有</param>
        /// <returns></returns>
        Dictionary<string, string> GetEventHandlers(string name="");
    }
}
