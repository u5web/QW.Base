﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 延迟事件声明
    /// </summary>
    public interface IDelayEvent : IEvent
    {
        /// <summary>
        /// 触发时间
        /// </summary>
        DateTime TriggerTime { get; }
    }
    /// <summary>
    /// 延迟事件声明
    /// </summary>
    /// <typeparam name="TSender"></typeparam>
    public interface IDelayEvent<TSender> : IDelayEvent {

        /// <summary>
        /// 触发事件源
        /// </summary>
        TSender Sender { get; }
    }
}
