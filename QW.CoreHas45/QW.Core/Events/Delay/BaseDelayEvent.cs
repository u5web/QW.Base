﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Events
{
    /// <summary>
    /// 延时事件基础模型
    /// </summary>
    public class BaseDelayEvent : IDelayEvent
    {
        /// <summary>
        /// 事件编号
        /// </summary>
        [JsonIgnore]
        public Guid Identity { get; protected set; }
        /// <summary>
        /// 调用时间
        /// </summary>
        [JsonIgnore]
        public DateTime EventTime { get; protected set; }
       
        /// <summary>
        /// 触发时间
        /// </summary>
        [JsonIgnore]
        public DateTime TriggerTime { get; protected set; }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="delay">延迟时间(分)</param>
        public BaseDelayEvent(TimeSpan delay)
        {
            this.Identity = Guid.NewGuid();
            this.EventTime = DateTime.Now;
            this.TriggerTime = this.EventTime.Add(delay);
        }
    }
    /// <summary>
    /// 延时事件基础模型
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class BaseDelayEvent<TKey> : BaseDelayEvent, IDelayEvent<TKey>
    {
        /// <summary>
        /// 事件源
        /// </summary>
        [JsonProperty]
        public TKey Sender { get; protected set; }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="delay">延迟时间(分)</param>
        public BaseDelayEvent(TKey sender, TimeSpan delay)
            : base(delay)
        {
            this.Sender = sender;
        }
    }
}
