﻿using QW.Core.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache
{
    /// <summary>
    /// 缓存处理器
    /// </summary>
    public class CacheHandler
    {
        private static ICacheHandler _cache = null;//缓存接口
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="cache"></param>
        internal static void Register(ICacheHandler cache)
        {
            _cache = cache;
        }
        /// <summary>
        /// 获取缓存默认过期时间
        /// </summary>
        public static int GetDefaultTimeOut()
        {
            return _cache.DefaultTimeOut;
        }
        /// <summary>
        /// 获取缓存前缀
        /// </summary>
        public static string GetPrefix()
        {
            return _cache.Prefix;
        }
        /// <summary>
        /// 获取完整Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetFullKey(string key)
        {
            return _cache.GetFullKey(key);
        }
        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            return _cache.Get<T>(key);
        }
        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="factory">取值工厂</param>
        /// <param name="span">缓存过期时间间隔,null使用默认缓存时间</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        /// <returns></returns>
        public static T Get<T>(string key, Func<T> factory, TimeSpan? span = null, bool isLongTerm = false)
        {
            var result = Get<T>(key);
            if (result == null)
            {
                result = factory();
                if (result != null)
                {
                    Set<T>(key, result, span, isLongTerm);
                }
            }
            return result;
        }

        /// <summary>
        /// 将指定键的对象添加到缓存中，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔,null使用默认缓存时间</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        public static void Set<T>(string key, T data, TimeSpan? span = null, bool isLongTerm = false)
        {
            _cache.Set<T>(key, data, span, isLongTerm);
        }
        /// <summary>
        /// 获得指定键的缓存集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static List<T> GetList<T>(string key)
        {
            return _cache.GetList<T>(key);
        }
        /// <summary>
        /// 获得指定键的缓存集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="factory">取值工厂</param>
        /// <param name="span">缓存过期时间间隔,null使用默认缓存时间</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        /// <returns></returns>
        public static List<T> GetList<T>(string key, Func<List<T>> factory, TimeSpan? span = null, bool isLongTerm = false)
        {
            var result = GetList<T>(key);
            if (result == null)
            {
                result = factory();
                if (result != null)
                {
                    SetList<T>(key, result, span, isLongTerm);
                }
            }
            return result;
        }

        /// <summary>
        /// 将缓存集合存入指定键，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔,null使用默认缓存时间</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        public static void SetList<T>(string key, IList<T> data, TimeSpan? span = null, bool isLongTerm = false)
        {
            _cache.SetList<T>(key, data, span, isLongTerm);
        }
        /// <summary>
        /// 从缓存中移除指定键的缓存值
        /// </summary>
        /// <param name="key"></param>
        public static void Remove(string key)
        {
            _cache.Remove(key);
        }
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return _cache.Exists(key);
        }

        #region 不是所有缓存方案都能提供这两个数据，故改为不用实现
        /*
        /// <summary>
        /// 缓存总数
        /// </summary>
        /// <returns></returns>
        public static int GetCount()
        {
            return _cache.GetCount();
        }
        /// <summary>
        /// 获取所有的key
        /// <para>会影响性能</para>
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllKeys()
        {
            return _cache.GetAllKeys();
        }
        */
        #endregion

    }
}
