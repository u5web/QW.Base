﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache.Memory
{
    /// <summary>
    /// 内存缓存处理程序构建器
    /// </summary>
    public static class MemoryCacheHandlerBuilder
    {
        /// <summary>
        /// 使用内存缓存处理程序
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static CacheHandlerBuilder UseMemoryCache(this CacheHandlerBuilder builder)
        {
            builder.RegisterHandler(new MemoryCacheHandler());
            return builder;
        }
    }
}
