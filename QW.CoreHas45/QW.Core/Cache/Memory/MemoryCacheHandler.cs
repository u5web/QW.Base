﻿using QW.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Reflection;
using System.Collections;
#if NETFM
using System.Web.Caching;
#endif
#if NETSD
using Microsoft.Extensions.Caching.Memory;
#endif

namespace QW.Core.Cache.Memory
{
    /// <summary>
    /// 内存缓存处理器
    /// </summary>
    public class MemoryCacheHandler : BaseCacheHandler, ICacheHandler
    {
#if NETFM
        /// <summary>
        /// 框架版缓存本体
        /// </summary>
        private System.Web.Caching.Cache _nf_cache;
#endif
#if NETSD
        /// <summary>
        /// 标准版缓存本体
        /// </summary>
        private IMemoryCache _ns_cache;
#endif
        /// <summary>
        /// .ctor
        /// </summary>
        public MemoryCacheHandler()
        {
#if NETFM
            _nf_cache = HttpRuntime.Cache;
#endif
#if NETSD
            _ns_cache = new MemoryCache(Microsoft.Extensions.Options.Options.Create(new MemoryCacheOptions { ExpirationScanFrequency = new TimeSpan(0, 0, 30) }));  //30秒清理一次过期缓存
#endif
        }
        /// <summary>
        /// 缓存总数
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            int result = 0;
#if NETFM
            result = _nf_cache.Count;
#endif
#if NETSD
            result = GetAllKeys().Count;
#endif
            return result;
        }
        /// <summary>
        /// 获取所有的key
        /// <para>会影响性能</para>
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllKeys()
        {
            List<string> result = new List<string>();
#if NETFM
            var cacheEnum = _nf_cache.GetEnumerator();
            while (cacheEnum.MoveNext()) { result.Add(cacheEnum.Key.ToString()); }
#endif
#if NETSD
            result = GetCacheKeys();
#endif
            return result;
        }
#if NETSD
        /// <summary>
        /// 获取所有缓存键
        /// </summary>
        /// <returns></returns>
        private List<string> GetCacheKeys()
        {
            const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            var entries = _ns_cache.GetType().GetField("_entries", flags).GetValue(_ns_cache);
            var cacheItems = entries as IDictionary;
            var keys = new List<string>();
            if (cacheItems == null) return keys;
            foreach (DictionaryEntry cacheItem in cacheItems)
            {
                keys.Add(cacheItem.Key.ToString());
            }
            return keys;
        }
#endif

        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            key = GetFinallyKey(key);
            T result = default(T);
#if NETFM
            object obj = null;
            obj = _nf_cache.Get(key);
            if (null != obj)
            {
                result = (T)obj;
            }
#endif
#if NETSD
            result = _ns_cache.Get<T>(key);
#endif
            return result;
        }

        /// <summary>
        /// 将指定键的对象添加到缓存中，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        public void Set<T>(string key, T data, TimeSpan? span = null, bool isLongTerm = false)
        {
            key = GetFinallyKey(key);
            var _ct = DateTimeOffset.Now.AddMinutes(DefaultTimeOut);
            if (span.HasValue)
            {
                _ct = DateTimeOffset.Now.Add(span.Value);
            }
#if NETFM
            if (!isLongTerm)
            {
                _nf_cache.Insert(key, data, null, _ct.DateTime, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            }
            else
            {
                _nf_cache.Insert(key, data, null);
            }
#endif
#if NETSD
            var option = new MemoryCacheEntryOptions { Priority = CacheItemPriority.High };
            if (!isLongTerm)
            {
                option.AbsoluteExpiration = _ct;
            }
            _ns_cache.Set(key, data, option);
#endif
        }
        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<T> GetList<T>(string key)
        {
            key = GetFinallyKey(key);
            List<T> result = default(List<T>);
#if NETFM
            object obj = null;
            obj = _nf_cache.Get(key);
            if (null != obj)
            {
                result = (List<T>)obj;
            }
#endif
#if NETSD
            result = _ns_cache.Get<List<T>>(key);
#endif
            return result;
        }

        /// <summary>
        /// 将缓存集合存入指定键，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        public void SetList<T>(string key, IList<T> data, TimeSpan? span = null, bool isLongTerm = false)
        {
            key = GetFinallyKey(key);
            var _ct = DateTimeOffset.Now.AddMinutes(DefaultTimeOut);
            if (span.HasValue)
            {
                _ct = DateTimeOffset.Now.Add(span.Value);
            }
#if NETFM
            if (!isLongTerm)
            {
                _nf_cache.Insert(key, data, null, _ct.DateTime, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.High, null);
            }
            else
            {
                _nf_cache.Insert(key, data, null);
            }
#endif
#if NETSD
            var option = new MemoryCacheEntryOptions { Priority = CacheItemPriority.High };
            if (!isLongTerm)
            {
                option.AbsoluteExpiration = _ct;
            }
            _ns_cache.Set(key, data, option);
#endif
        }
        /// <summary>
        /// 从缓存中移除指定键的缓存值
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            key = GetFinallyKey(key);
#if NETFM
            _nf_cache.Remove(key);
#endif
#if NETSD
            _ns_cache.Remove(key);
#endif
        }
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            bool result = false;
            key = GetFinallyKey(key);
#if NETFM
            if (_nf_cache.Get(key) != null)
            {
                result = true;
            }
#endif
#if NETSD
            object _ = new object();
            result = _ns_cache.TryGetValue(key, out _);
#endif
            return result;
        }
    }
}
