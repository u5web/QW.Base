﻿using CSRedis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache.Redis
{
    /// <summary>
    /// Redis缓存处理程序构建器
    /// </summary>
    public static class RedisCacheHandlerBuilder
    {
        /// <summary>
        /// 使用Redis缓存处理程序
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="connectionName">redis连接字符串</param>
        /// <returns></returns>
        public static CacheHandlerBuilder UseRedisCache(this CacheHandlerBuilder builder, string connectionName)
        {
            CSRedisClient redisClient = new CSRedisClient(connectionName);
            RedisHelper.Initialization(redisClient);
            return builder.UseRedisCache();
        }
        /// <summary>
        /// 使用Redis缓存处理程序
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="redisClient">RedisHelper.Initialization(redisClient)</param>
        /// <returns></returns>
        public static CacheHandlerBuilder UseRedisCache(this CacheHandlerBuilder builder, CSRedisClient redisClient)
        {
            RedisHelper.Initialization(redisClient);
            return builder.UseRedisCache();
        }
        /// <summary>
        /// 使用Redis缓存处理程序
        /// <para>需要手动RedisHelper.Initialization(redisClient)</para>
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static CacheHandlerBuilder UseRedisCache(this CacheHandlerBuilder builder)
        {
            builder.RegisterHandler(new RedisCacheHandler());
            return builder;
        }
    }
}
