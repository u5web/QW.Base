﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Exceptions
{
    /// <summary>
    /// 基础异常类
    /// </summary>
    public class QWBaseException : ApplicationException
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public QWBaseException() { }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        public QWBaseException(string message) : base(message) { }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public QWBaseException(string message, Exception inner) : base(message, inner) { }
    }
}
