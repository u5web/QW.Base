﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace QW.Core.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public class IOHelper
    {
#if NETSD
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetMapPath(string path)
        {
            /*
             * https://www.cnblogs.com/jhxk/articles/9304943.html
             * 两个结果等价
            String basePath1 = AppContext.BaseDirectory;
            String basePath2 =Path.GetDirectoryName(typeof(Program).Assembly.Location);
            */
            var _basepath = AppContext.BaseDirectory;
            string result = PathCombine(_basepath, path);
            return result;
        }
#endif

#if NETFM
        /// <summary>
        /// 获取当前目录
        /// （网站为网站根目录，测试时为dll所在目录）
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetMapPath(string path)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Server.MapPath(path);
            }
            else
            {
                string root = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                /*
                char splitchar = Path.DirectorySeparatorChar;
                if (!string.IsNullOrWhiteSpace(path))
                {
                    path = path.Replace('/', splitchar);
                }
                */
                return PathCombine(root, path);
            }
        }
#endif
        /// <summary>
        /// 路径合并
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <returns></returns>
        public static string PathCombine(string path1, string path2)
        {
            var arrPath = (path1 + '/' + path2).Split('/');
            return Path.Combine(arrPath);
            /*
            string sp = @"\", back_sp = @"/"; ;
            if (path1.IndexOf(sp) < 0)
            {
                sp = @"/";
                back_sp = @"\";
            }
            path2 = path2.Replace(back_sp, sp);
            path1 = path1.Replace(back_sp, sp);
            if (path2.StartsWith(sp))
            {
                path2 = path2.Substring(1);
            }
            if (!path1.EndsWith(sp))
            {
                path1 += sp;
            }
            return path1 + path2;
            */
        }
    }
}
