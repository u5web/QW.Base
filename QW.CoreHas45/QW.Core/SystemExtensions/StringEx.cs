﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace System
{
    /// <summary>
    /// 字符串扩展方法
    /// </summary>
    public static class StringEx
    {
        /// <summary>
        /// 补全前后分隔符
        /// </summary>
        /// <param name="str"></param>
        /// <param name="delimiter">分隔符</param>
        /// <returns></returns>
        public static string DelimiterCorrect(this string str, string delimiter = ",")
        {
            var split_len = delimiter.Length;
            if (!string.IsNullOrEmpty(str))
            {
                if (str.Length > split_len)
                {
                    if (str.Substring(0, split_len) != delimiter) str = delimiter + str;
                    if (str.Substring(str.Length - split_len, split_len) != delimiter) str += delimiter;
                }
                else
                {
                    str = delimiter + str;
                    str += delimiter;
                }
            }
            return str;
        }
        /// <summary>
        /// 清理前后分隔符
        /// </summary>
        /// <param name="str"></param>
        /// <param name="delimiter">分隔符</param>
        /// <returns></returns>
        public static string DelimiterTrim(this string str, string delimiter = ",")
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = Regex.Replace(str, "^" + delimiter + "+", "");
                str = Regex.Replace(str, delimiter + "+$", "");
            }
            return str;
        }
        /// <summary>
        /// 生成重复字符串
        /// </summary>
        /// <param name="str">字符</param>
        /// <param name="length">数量</param>
        /// <returns></returns>
        public static string Repeat(this string str, int length)
        {
            if (length > 0)
            {
                StringBuilder _sb = new StringBuilder(length * str.Length);
                for (int i = 1; i < length; i++)
                {
                    _sb.Append(str);
                }
                return _sb.ToString();
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        /// 截取字符,并加上后缀
        /// </summary>
        /// <param name="str"></param>
        /// <param name="max_length">长度,大于0</param>
        /// <param name="suffix">后缀</param>
        /// <returns></returns>
        public static string Cut(this string str, int max_length, string suffix = "...")
        {
            string result = str;
            if (!string.IsNullOrEmpty(result) && result.Length > max_length)
            {
                result = result.Substring(0, max_length) + suffix;
            }
            return result;
        }
        /// <summary>
        /// br换为换行(默认\n)
        /// </summary>
        /// <param name="str"></param>
        /// <param name="brstr">替换字符</param>
        /// <returns></returns>
        public static string BR2NewLine(this string str, string brstr = "\n")
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = Regex.Replace(str, "<br[^>]*>", brstr);
            }
            return str;
        }
        /// <summary>
        /// \n换行换成br
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string NewLine2BR(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = Regex.Replace(str, "\r\n", "<br />");
                str = Regex.Replace(str, "\r", "<br />");
                str = Regex.Replace(str, "\n", "<br />");
            }
            return str;
        }
        /// <summary>
        /// 清理换行
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ClearNewLine(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = Regex.Replace(str, "\r\n", "");
                str = Regex.Replace(str, "\r", "");
                str = Regex.Replace(str, "\n", "");
            }
            return str;
        }
        /// <summary>
        /// 编码汉字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Escape(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                sb.Append((Char.IsLetterOrDigit(c) || c == '-' || c == '_' || c == '\\' || c == '/' || c == '.') ? c.ToString() : Uri.HexEscape(c));
            }
            return sb.ToString();
        }
        /// <summary>
        /// 解码汉字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string UnEscape(this string str)
        {
            StringBuilder sb = new StringBuilder();
            int len = str.Length;
            int i = 0;
            while (i != len)
            {
                if (Uri.IsHexEncoding(str, i)) sb.Append(Uri.HexUnescape(str, ref i));
                else sb.Append(str[i++]);
            }
            return sb.ToString();
        }
        /// <summary>
        /// 邮箱格式验证
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool isEmail(this string str)
        {
            bool ismail = false;
            if (!string.IsNullOrWhiteSpace(str))
            {
                string emailpattren = @"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))";
                if (Regex.IsMatch(str, emailpattren))
                {
                    ismail = true;
                }
            }
            return ismail;
        }
        /// <summary>
        /// 半角转全角
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToSBC(string input)
        {
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 32)
                {
                    c[i] = (char)12288;
                    continue;
                }
                if (c[i] < 127)
                    c[i] = (char)(c[i] + 65248);
            }
            return new string(c);
        }
        /// <summary>
        ///  全角转半角
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns></returns>
        public static string ToDBC(string input)
        {
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }
                if (c[i] > 65280 && c[i] < 65375)
                    c[i] = (char)(c[i] - 65248);
            }
            return new string(c);
        }
    }
}
