﻿using System;
using System.Web;
#if NETSD
using Microsoft.AspNetCore.Http;
#endif

namespace QW.Core.Web
{
    /// <summary>
    /// 网页辅助
    /// </summary>
    public class Cookie
    {
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="name"></param>
        public static void Delete(string name)
        {
#if NETSD
            HttpContext.Current.Response.Cookies.Delete(name);
#endif
#if NETFM
            HttpContext.Current.Response.Cookies.Remove(name);
#endif
        }
        /// <summary>
        /// 获取指定Cookie值
        /// </summary>
        /// <param name="name">cookiename</param>
        /// <returns></returns>
        public static string GetValue(string name)
        {
            string result = string.Empty;
#if NETSD
            HttpContext.Current.Request.Cookies.TryGetValue(name, out result);
#endif
#if NETFM

            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
            if (cookie != null)
            {
                result = cookie.Value;
            }
#endif

            return result;
        }
        /// <summary>
        /// 添加一个Cookie
        /// </summary>
        /// <param name="name">cookie名</param>
        /// <param name="value">cookie值</param>
        /// <param name="expires">过期时间 DateTime</param>
        /// <param name="domain">域</param>
        public static void SetValue(string name, string value, DateTime? expires = null, string domain = "")
        {

#if NETSD
            bool needcko = false;
            var cko = new CookieOptions();
            if (expires != null)
            {
                needcko = true;
                cko.Expires = expires;
            }
            if (!string.IsNullOrWhiteSpace(domain))
            {
                needcko = true;
                cko.Domain = domain;
            }
            if (needcko)
            {
                HttpContext.Current.Response.Cookies.Append(name, value, cko);
            }
            else
            {
                HttpContext.Current.Response.Cookies.Append(name, value);
            }

#endif
#if NETFM
            HttpCookie cookie = new HttpCookie(name);
            cookie.Value = value;
            if (expires != null)
            {
                cookie.Expires = (DateTime)expires;
            }
            if (!string.IsNullOrWhiteSpace(domain))
            {
                cookie.Domain = domain;
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
#endif
        }
    }
}
