﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if NETSD
using Microsoft.AspNetCore.Http;
#endif
#if NETFM
using System.Web;
#endif

namespace QW.Core.Web
{
#if NETSD
    /// <summary>
    /// Http上下文扩展
    /// </summary>
    public class HttpContext
    {
        private static IHttpContextAccessor _accessor;
        /// <summary>
        /// 当前上下文
        /// </summary>
        public static Microsoft.AspNetCore.Http.HttpContext Current
        {
            get
            {
                return _accessor.HttpContext;
            }
        }
        /// <summary>
        /// 注入上下文
        /// </summary>
        /// <param name="accessor"></param>
        public static void Configure(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        /// <summary>
        /// 获取全长url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetAbsoluteUri(HttpRequest request)
        {
            return new StringBuilder()
                .Append(request.Scheme)
                .Append("://")
                .Append(request.Host)
                .Append(request.PathBase)
                .Append(request.Path)
                .Append(request.QueryString)
                .ToString();
        }
        /// <summary>
        /// 获取全长url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetBaseUri(HttpRequest request)
        {
            return new StringBuilder()
                .Append(request.Scheme)
                .Append("://")
                .Append(request.Host)
                .ToString();
        }
    }
#endif
#if NETFM
    /// <summary>
    /// Http上下文扩展
    /// </summary>
    public class HttpContext
    {
        /// <summary>
        /// 当前上下文
        /// </summary>
        public static System.Web.HttpContext Current
        {
            get
            {
                return System.Web.HttpContext.Current;
            }
        }
        /// <summary>
        /// 获取全长url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetAbsoluteUri(HttpRequest request)
        {
            return new StringBuilder()
                .Append(request.Url.Scheme)
                .Append("://")
                .Append(request.Url.Host)
                .Append(request.Path)
                .Append(request.QueryString)
                .ToString();
        }
        /// <summary>
        /// 获取全长url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetBaseUri(HttpRequest request)
        {
            return new StringBuilder()
                .Append(request.Url.Scheme)
                .Append("://")
                .Append(request.Url.Host)
                .ToString();
        }
    }
#endif
}
