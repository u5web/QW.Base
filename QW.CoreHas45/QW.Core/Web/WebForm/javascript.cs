﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if NETFM
namespace QW.Core.Web.WebForm
{
    /// <summary>
    /// 前台javascript交互脚本控件
    /// </summary>
    public class javascript
    {
        /// <summary>
        /// 引入脚本文件到页面
        /// </summary>
        /// <param name="filepath">文件地址</param>
        /// <param name="page">null会自动加载到当前页</param>
        /// <param name="isEnd">代码是否是嵌入在页面的底部、表单的最后,为true时适用于要在页面控件加载完成后运行的JS代码</param>
        public static void Register(string filepath, System.Web.UI.Page page = null, bool isEnd = false)
        {
            if (page == null)
            {
                page = System.Web.HttpContext.Current.Handler as System.Web.UI.Page;
            }
            if (!isEnd)
            {
                if (!page.ClientScript.IsStartupScriptRegistered(page.GetType(), "clientScript"))
                {
                    page.ClientScript.RegisterStartupScript(page.GetType(), "clientScript", "<script type=\"text/javascript\" src=\"" + filepath + "\"></script>");
                }
            }
            else
            {
                if (!page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), "clientScript"))
                {
                    page.ClientScript.RegisterClientScriptBlock(page.GetType(), "clientScript", "<script type=\"text/javascript\" src=\"" + filepath + "\"></script>");
                }
            }
        }

    #region 自定义脚本代码

        /// <summary>
        /// 自定义脚本在页面的顶部、表单的最前，适用于要在控件加载前执行的JS代码
        /// </summary>
        /// <param name="content">脚本内容</param>
        public static void Custom(string content)
        {
            System.Web.UI.Page page = System.Web.HttpContext.Current.Handler as System.Web.UI.Page;
            Custom(page, content);
        }
        /// <summary>
        /// 自定义脚本在页面的顶部、表单的最前，适用于要在控件加载前执行的JS代码
        /// </summary>
        /// <param name="page">System.Web.UI.Page   如果是当前页写this</param>
        /// <param name="content">脚本内容</param>
        public static void Custom(System.Web.UI.Page page, string content)
        {
            if (!page.ClientScript.IsStartupScriptRegistered(page.GetType(), "clientScript"))
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), "clientScript", "<script type=\"text/javascript\">" + content + "</script>");
            }
        }
        /// <summary>
        /// 自定义脚本在页面的底部、表单的最后，适用于要在页面控件加载完成后运行的JS代码
        /// </summary>
        /// <param name="content">脚本内容</param>
        public static void CustomEnd(string content)
        {
            System.Web.UI.Page page = System.Web.HttpContext.Current.Handler as System.Web.UI.Page;
            CustomEnd(page, content);
        }
        /// <summary>
        /// 自定义脚本
        /// *JS代码嵌入在页面的底部、表单的最后，适用于要在页面控件加载完成后运行的JS代码
        /// </summary>
        /// <param name="page">System.Web.UI.Page   如果是当前页写this</param>
        /// <param name="content">脚本内容</param>
        public static void CustomEnd(System.Web.UI.Page page, string content)
        {
            if (!page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), "clientScript"))
            {
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), "clientScript", "<script type=\"text/javascript\">" + content + "</script>");
            }
        }
    #endregion 自定义脚本代码
        
        /// <summary>
        /// 提示框(Alert)
        /// </summary>
        /// <param name="page">System.Web.UI.Page   如果是当前页写this</param>
        /// <param name="content">用户提示信息</param>
        /// <param name="toPage">跳转页面地址</param>
        /// <param name="frameName">跳转框架名,~top表示顶层框架,~h表示返回上一页</param>
        /// <param name="isEnd">代码是否是嵌入在页面的底部、表单的最后,为true时适用于要在页面控件加载完成后运行的JS代码</param>
        public static void Alert(System.Web.UI.Page page, string content, string toPage = "", string frameName = "", bool isEnd = false)
        {
            if (!string.IsNullOrEmpty(content))
            {
                string scriptcontent;
                content = content.Replace("\"", "\\\"");
                toPage = toPage.Replace("\"", "\\\"");
                scriptcontent = "alert(\"" + content + "\");";
                if (!string.IsNullOrWhiteSpace(toPage))
                {
                    frameName = "~h";
                }
                if (!string.IsNullOrWhiteSpace(frameName))
                {
                    switch (frameName)
                    {
                        case "~top":
                            scriptcontent += "top.location.href=\"";
                            break;
                        case "~h":
                            scriptcontent += "history.back(1);";
                            break;
                        default:
                            scriptcontent += "parent." + frameName + ".location.href=\"";
                            break;
                    }
                }
                else
                {
                    scriptcontent += "location.href=\"";
                }
                if (frameName != "~h")
                {
                    scriptcontent += toPage + "\"";
                }
                if (isEnd)
                {
                    Custom(page, scriptcontent);
                }
                else
                {
                    CustomEnd(page, scriptcontent);
                }
            }
        }
        
        /// <summary>
        /// 页面跳转
        /// </summary>
        /// <param name="page">System.Web.UI.Page   如果是当前页写this</param>
        /// <param name="toPage">跳转页面地址</param>
        /// <param name="frameName">跳转框架名,~top表示顶层框架,~h表示返回上一页,为空表示当前框架跳转</param>
        /// <param name="isEnd">代码是否是嵌入在页面的底部、表单的最后,为true时适用于要在页面控件加载完成后运行的JS代码</param>
        public static void ToPage(System.Web.UI.Page page, string toPage = "", string frameName = "", bool isEnd = false)
        {
            if (!string.IsNullOrEmpty(toPage))
            {
                string scriptcontent;
                toPage = toPage.Replace("\"", "\\\"");
                scriptcontent = "";
                if (!string.IsNullOrWhiteSpace(toPage))
                {
                    frameName = "~h";
                }
                if (!string.IsNullOrWhiteSpace(frameName))
                {
                    switch (frameName)
                    {
                        case "~top":
                            scriptcontent += "top.location.href=\"";
                            break;
                        case "~h":
                            scriptcontent += "history.back(1);";
                            break;
                        default:
                            scriptcontent += "parent." + frameName + ".location.href=\"";
                            break;
                    }
                }
                else
                {
                    scriptcontent += "location.href=\"";
                }
                if (frameName != "~h")
                {
                    scriptcontent += toPage + "\"";
                }
                if (isEnd)
                {
                    Custom(page, scriptcontent);
                }
                else
                {
                    CustomEnd(page, scriptcontent);
                }
            }
        }
    }
}
#endif
