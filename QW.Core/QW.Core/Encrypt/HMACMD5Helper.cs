using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using QW.Core.Exceptions;
using System.IO;

namespace QW.Core.Encrypt
{
    /// <summary>
    /// HMACMD5辅助工具
    /// </summary>
    public static class HMACMD5Helper
    {
        #region MD5
        /// <summary>
        /// 获取Hash
        /// <para>默认UTF8</para>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="encode"></param>
        /// <returns></returns>
        public static string GetHMACMD5(this string str, string key, Encoding encode = null)
        {
            if (encode == null)
            {
                encode = Encoding.UTF8;
            }
            byte[] fromData;
            fromData = encode.GetBytes(str);
            return GetHMACMD5(fromData, key);
        }

        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHMACMD5(this Stream stream, string key)
        {
            HMACMD5 md5 = new HMACMD5(Encoding.Default.GetBytes(key));
            byte[] hash = md5.ComputeHash(stream);
            md5.Clear();
            return MD5Helper.GetHashCode(hash, MD5EncryptLengthType.Long);
        }
        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHMACMD5(this byte[] bytes, string key)
        {
            HMACMD5 md5 = new HMACMD5(Encoding.Default.GetBytes(key));
            byte[] hash = md5.ComputeHash(bytes);
            md5.Clear();
            return MD5Helper.GetHashCode(hash, MD5EncryptLengthType.Long);
        }
        #endregion

        #region MD5Base64
        /// <summary>
        /// 获取Hash并Base64化
        /// <para>默认UTF8</para>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="encode"></param>
        /// <returns></returns>
        public static string GetHMACMD5Base64(this string str, string key, Encoding encode = null)
        {
            string result = GetHMACMD5(str, key, encode);
            return MD5Helper.GetHashCodeToBase64(result);
        }
        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHMACMD5Base64(this Stream stream, string key)
        {
            string result = GetHMACMD5(stream, key);
            return MD5Helper.GetHashCodeToBase64(result);
        }
        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHMACMD5Base64(this byte[] bytes, string key)
        {
            string result = GetHMACMD5(bytes, key);
            return MD5Helper.GetHashCodeToBase64(result);
        }
        #endregion
    }
}
