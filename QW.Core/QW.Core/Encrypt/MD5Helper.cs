using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using QW.Core.Exceptions;
using System.IO;

namespace QW.Core.Encrypt
{
    /// <summary>
    /// MD5辅助工具
    /// </summary>
    public static class MD5Helper
    {
        #region MD5
        /// <summary>
        /// 获取Hash
        /// <para>默认UTF8</para>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ltype"></param>
        /// <param name="encode"></param>
        /// <returns></returns>
        public static string GetMD5(this string str, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long, Encoding encode = null)
        {
            if (encode == null)
            {
                encode = Encoding.UTF8;
            }
            byte[] fromData;
            fromData = encode.GetBytes(str);
            return GetMD5(fromData, ltype);
        }

        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="ltype"></param>
        /// <returns></returns>
        public static string GetMD5(this Stream stream, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(stream);
            return GetHashCode(hash, ltype);
        }
        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="ltype"></param>
        /// <returns></returns>
        public static string GetMD5(this byte[] bytes, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(bytes);
            return GetHashCode(hash, ltype);
        }
        #endregion

        #region MD5Base64
        /// <summary>
        /// 获取Hash并Base64化
        /// <para>默认UTF8</para>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ltype"></param>
        /// <param name="encode"></param>
        /// <returns></returns>
        public static string GetMD5Base64(this string str, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long, Encoding encode = null)
        {
            string result = GetMD5(str, ltype, encode);
            return GetHashCodeToBase64(result);
        }
        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="ltype"></param>
        /// <returns></returns>
        public static string ComputeHashToBase64(this Stream stream, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long)
        {
            string result = GetMD5(stream, ltype);
            return GetHashCodeToBase64(result);
        }
        /// <summary>
        /// 获取Hash
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="ltype"></param>
        /// <returns></returns>
        public static string ComputeHashToBase64(this byte[] bytes, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long)
        {
            string result = GetMD5(bytes, ltype);
            return GetHashCodeToBase64(result);
        }
        #endregion

        #region 内部方法
        /// <summary>
        /// 获取Hash字符
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="ltype"></param>
        /// <returns></returns>
        internal static string GetHashCode(byte[] bytes, MD5EncryptLengthType ltype = MD5EncryptLengthType.Long)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("x2"));
            }
            string result = sb.ToString();
            if (ltype == MD5EncryptLengthType.Short)
            {
                result = result.Substring(8, 16);
            }
            return result.ToString();
        }
        /// <summary>
        /// 获取Hash字符(base64)
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        internal static string GetHashCodeToBase64(string hash)
        {
            string result = hash;
            List<byte> lstBytes = new List<byte>();
            for (int i = 0; i < result.Length; i += 2)
            {
                lstBytes.Add(Convert.ToByte(result.Substring(i, 2), 16));
            }
            result = Convert.ToBase64String(lstBytes.ToArray());
            return result;
        }
        #endregion
    }
    /// <summary>
    /// Md5加密字符串长度
    /// </summary>
    public enum MD5EncryptLengthType
    {
        /// <summary>
        /// 16位
        /// </summary>
        Short = 16,
        /// <summary>
        /// 32位
        /// </summary>
        Long = 32
    }
}
