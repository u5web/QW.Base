﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Encrypt
{
    /// <summary>
    /// 常量数据
    /// </summary>
    internal class ConstData
    {
        /// <summary>
        /// 默认加密解密key
        /// </summary>
        internal const string DEFAULT_KEY = "qw_encrypt_key";
        /// <summary>
        /// 默认加密解密IV
        /// </summary>
        internal const string DEFAULT_IV = "qw_encrypt_iv";
    }
}
