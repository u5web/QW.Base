﻿using QW.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core
{
    /// <summary>
    /// 实例
    /// </summary>
    public class Instance
    {
        /// <summary>
        /// 获取实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="classFullName"></param>
        /// <returns></returns>
        public static T Get<T>(string classFullName)
        {
            try
            {
                Type sourceType = Type.GetType(classFullName);
                return (T)Activator.CreateInstance(sourceType);
            }
            catch (Exception ex)
            {
                throw new InstanceCreateException("创建实例异常", ex);
            }
        }

    }
}
