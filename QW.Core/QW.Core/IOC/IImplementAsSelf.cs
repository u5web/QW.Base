﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 以自身实例注入
    /// </summary>
    public interface IImplementAsSelf
    {
    }
}
