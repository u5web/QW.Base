﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.IOC
{
    /// <summary>
    /// 单例注入约束
    /// </summary>
    public interface ISingletonDependency : IDependency
    {
    }
}
