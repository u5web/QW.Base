﻿using System;
using System.Web;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace QW.Core.Helper
{
    /// <summary>
    /// 网页辅助
    /// </summary>
    public class WebHelper
    {
        #region IP地址
        /// <summary>
        /// 获取当前客户IP
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentIp()
        {
            string result = String.Empty;
            result = QW.Core.Web.HttpContext.Current.Connection.RemoteIpAddress.ToString();
            if (string.IsNullOrEmpty(result))
            {
                return "127.0.0.1";
            }
            return result;
        }
        #endregion
        
    }
}