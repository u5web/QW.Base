﻿using System;
using System.Collections.Generic;

namespace QW.Core.AutoTasks
{
    /// <summary>
    /// 任务中心接口
    /// </summary>
    public interface ITaskCenter : IDisposable
    {
        /// <summary>
        /// 注册任务
        /// </summary>
        /// <param name="tasks"></param>
        void Register(IEnumerable<ITask> tasks);
        /// <summary>
        /// 修改触发条件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="interval"></param>
        void ChangeTrigger(string name, TaskInterval interval);
        /// <summary>
        /// 启动
        /// </summary>
        void Start();
        /// <summary>
        /// 停止
        /// </summary>
        void Stop();
    }
}
