﻿namespace QW.Core.AutoTasks
{
    /// <summary>
    /// 任务周期类型
    /// </summary>
    public enum TaskIntervalType
    {
        /// <summary>
        /// 按秒(每多少秒执行一次)
        /// </summary>
        Seconds = 1,
        /// <summary>
        /// 按天(一天只执行一次)
        /// </summary>
        Daily = 2,
        /// <summary>
        /// 按Cron表达式
        /// </summary>
        Cron = 3,
    }
}
