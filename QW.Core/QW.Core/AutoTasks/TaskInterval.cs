﻿using System;

namespace QW.Core.AutoTasks
{
    /// <summary>
    /// 任务周期
    /// </summary>
    public class TaskInterval
    {
        /// <summary>
        /// 周期类型
        /// </summary>
        public TaskIntervalType Type { get; set; }
        /// <summary>
        /// 秒间隔值
        /// </summary>
        public int SecondsValue { get; set; }
        /// <summary>
        /// 每天触发时间(时分)
        /// </summary>
        public TimeSpan DailyValue { get; set; }
        /// <summary>
        /// Cron表达式
        /// </summary>
        public string CronExpression { get; set; }
    }
}
