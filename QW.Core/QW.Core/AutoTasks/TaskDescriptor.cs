﻿using System;
using QW.Core;

namespace QW.Core.AutoTasks
{
    /// <summary>
    /// 任务描述
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TaskDescriptor<T>
    {
        /// <summary>
        /// 键
        /// </summary>
        public T Key { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 分组
        /// </summary>
        public string Group { get; set; }
        /// <summary>
        /// 定时任务方法实例
        /// </summary>
        public ITask Instance { get; set; }
        /// <summary>
        /// 处理器
        /// </summary>
        public FastInvoke.FastInvokeHandler Handler { get; set; }
        /// <summary>
        /// 运行周期
        /// </summary>
        public TaskInterval Interval { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public Type Type { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public bool Logger { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public bool Locker { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public int LockerTimeOut { get; internal set; }

    }
}
