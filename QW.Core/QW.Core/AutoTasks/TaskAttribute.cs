﻿using System;

namespace QW.Core.AutoTasks
{
    /// <summary>
    /// 任务注解
    /// </summary>
    public class TaskAttribute : Attribute
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 任务描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 分组(默认:Default)
        /// </summary>
        public string Group { get; } = "Default";
        /// <summary>
        /// 
        /// </summary>
        public bool Logger { get; set; } = false;
        /// <summary>
        /// 分布式锁
        /// </summary>
        public bool Locker { get; set; } = false;
        /// <summary>
        /// 
        /// </summary>
        public int LockerTimeOut { get; set; } = 60;
        /// <summary>
        /// 任务周期
        /// </summary>
        public TaskInterval Interval { get; }
        /// <summary>
        /// Cron表达式执行
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cronExpression">cron表达式</param>
        public TaskAttribute(string name, string cronExpression)
        {
            Name = name;
            Interval = new TaskInterval
            {
                Type = TaskIntervalType.Cron,
                CronExpression = cronExpression
            };
        }
        /// <summary>
        /// 按间隔秒数执行
        /// </summary>
        /// <param name="name"></param>
        /// <param name="secondsValue">秒间隔值</param>
        public TaskAttribute(string name, int secondsValue)
        {
            Name = name;
            Interval = new TaskInterval
            {
                Type = TaskIntervalType.Seconds,
                SecondsValue = secondsValue
            };
        }
        /// <summary>
        /// 每天执行一次
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dailyValue">每天触发时分</param>
        public TaskAttribute(string name, TimeSpan dailyValue)
        {
            Name = name;
            Interval = new TaskInterval
            {
                Type = TaskIntervalType.Daily,
                DailyValue = dailyValue
            };
        }
    }
}
