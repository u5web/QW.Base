﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.CommonModel
{
    /// <summary>
    /// 通用分页数据返回
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageData<T>
    {
        /// <summary>
        /// 当页数据集
        /// </summary>
        public List<T> rows { get; set; }
        /// <summary>
        /// 总数
        /// </summary>
        public long total { get; set; }
        /// <summary>
        /// 当前页
        /// </summary>
        public int page_index { get; set; }
        /// <summary>
        /// 每页数量
        /// </summary>
        public int page_size { get; set; }
    }
}
