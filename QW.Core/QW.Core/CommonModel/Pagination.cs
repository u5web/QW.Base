﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace QW.Core.CommonModel
{
    /// <summary>
    /// 分页计算
    /// </summary>
    public class Pagination
    {
        /// <summary>
        /// 默认每页记录数
        /// </summary>
        private const int DEFAULT_PAGESIZE = 10;

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="recordcount">总记录条数</param>
        /// <param name="page">当前页</param>
        /// <param name="pagesize">每页显示数量</param>
        public Pagination(long recordcount, int page = 1, int pagesize = DEFAULT_PAGESIZE)
        {
            RecordCount = recordcount;
            PageSize = pagesize;

            if (page == 0)
            {
                throw new Exception("页码不能从0开始");
            }
            CurrentPage = page;
        }
        #endregion

        /// <summary>
        /// 每页显示数量(私有)
        /// </summary>
        private int _PageSize = DEFAULT_PAGESIZE;
        /// <summary>
        /// 每页显示数量
        /// </summary>
        public int PageSize
        {
            get
            {
                return _PageSize;
            }
            set
            {
                if (value > 0)
                {
                    _PageSize = value;
                }
                else
                {
                    //_PageSize = DEFAULT_PAGESIZE;
                    throw new Exception("每页显示数不可为零或负数");
                }
            }
        }
        /// <summary>
        /// 总记录条数(私有)
        /// </summary>
        private long _RecordCount = 0;
        /// <summary>
        /// 总记录条数
        /// </summary>long
        public long RecordCount
        {
            get
            {
                if (_RecordCount < 0) _RecordCount = 0;
                return _RecordCount;
            }
            set
            {
                if (value > 0)
                {
                    _RecordCount = value;
                }
                else
                {
                    _RecordCount = 0;
                }
            }
        }
        /// <summary>
        /// 最大页数
        /// </summary>
        public int MaxPage
        {
            get
            {
                int tmp = (int)Math.Ceiling(RecordCount / (double)PageSize);
                if (tmp < 1) tmp = 1;
                return tmp;
            }
        }
        /// <summary>
        /// 当前页(私有)
        /// </summary>
        private int _CurrentPage = 1;
        /// <summary>
        /// 当前页
        /// </summary>
        public int CurrentPage
        {
            get
            {
                //处理下限
                //if (_CurrentPage < 1) _CurrentPage = 1;
                return _CurrentPage;
            }
            set
            {
                _CurrentPage = value;
            }
        }
        /// <summary>
        /// 数据获取开始索引
        /// </summary>
        public int StartIndex
        {
            get
            {
                int result = 0;
                result = (CurrentPage - 1) * PageSize;
                return result;
            }
        }
        /// <summary>
        /// 数据获取结束索引
        /// </summary>
        public int EndIndex
        {
            get
            {
                int result = 0;
                result = CurrentPage * PageSize;
                return result;
            }
        }
        /// <summary>
        /// 当前页值是否正确
        /// </summary>
        public bool IsTrueCurrentPage
        {
            get
            {
                return CurrentPage > 0 && CurrentPage <= MaxPage;
            }
        }
    }
}
