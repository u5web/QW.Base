﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//一些通用的请求参数模型
namespace QW.Core.CommonModel
{
    /// <summary>
    /// 简单数据请求参数模型
    /// </summary>
    public class SimpleDataParameter<T>
    {
        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }
    }
    /// <summary>
    /// 简单数据集请求参数模型
    /// </summary>
    public class SimpleDataListParameter<T>
    {
        /// <summary>
        /// 数据集
        /// </summary>
        public List<T> Datas { get; set; }
    }
}
