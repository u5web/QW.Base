﻿using System;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace QW.Core.RunHelper
{
    /// <summary>
    /// Windows 服务
    /// </summary>
    internal class WinService : ServiceBase
    {
        private static string WinServiceName;

        private static Action<string[]> StartRun;

        public WinService()
        {
            ServiceName = WinServiceName;
        }

        /// <summary>
        /// 启动服务
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            OnRun(args);
        }

        protected async Task OnRun(string[] args)
        {
            await Task.Run(delegate
            {
                StartRun(args);
            });
        }

        public static void Config(Action<string[]> startRun, string serviceName)
        {
            WinServiceName = serviceName;
            StartRun = startRun;
        }
    }
}
