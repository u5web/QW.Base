﻿using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found
{
    /// <summary>
    /// RuntimeHelper
    /// </summary>
    public class RuntimeHelper
    {
        /// <summary>
        /// 获取项目程序集，排除所有的系统程序集(Microsoft.***、System.***等)、Nuget下载包
        /// </summary>
        /// <returns></returns>
        public static IList<Assembly> GetAllAssemblies()
        {
            var list = new List<Assembly>();
            var deps = DependencyContext.Default;
            var libs = deps.CompileLibraries.Where(lib => !lib.Serviceable && lib.Type != "package").ToList();//排除所有的系统程序集、Nuget下载包
            foreach (var lib in libs)
                try
                {
                    var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(lib.Name));
                    list.Add(assembly);
                }
                catch (Exception)
                {
                    // ignored
                }
            return list;
        }
        /// <summary>
        /// 获取程序集
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static Assembly GetAssembly(string assemblyName)
        {
            return GetAllAssemblies().FirstOrDefault(assembly => assembly.FullName.Contains(assemblyName));
        }

        private static List<Type> projectTypes;
        /// <summary>
        /// 获取项目类型集
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetProjectTypes()
        {
            if (projectTypes == null)
            {
                projectTypes = new List<Type>();
                foreach (var assembly in GetProjectAssembly())
                {
                    var typeInfos = assembly.DefinedTypes;
                    foreach (var typeInfo in typeInfos)
                        projectTypes.Add(typeInfo.AsType());
                }
            }
            return projectTypes;
        }
        /// <summary>
        /// 清理项目类型集
        /// </summary>
        public static void ClearProjectTypes()
        {
            projectTypes?.Clear();
            projectTypes = null;
        }
        /// <summary>
        /// 获取项目程序集
        /// </summary>
        /// <returns></returns>
        public static List<Assembly> GetProjectAssembly(string filter = null)
        {
            var result = new List<Assembly>();
            var projects = DependencyContext.Default.CompileLibraries.Where(p => p.Type == "project").ToList();
            foreach (var project in projects)
            {
                if (!string.IsNullOrEmpty(filter) && !project.Name.Contains(filter))
                    continue;
                var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(project.Name));
                result.Add(assembly);
            }
            return result;
        }
        /// <summary>
        /// 获取所有类型集
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetAllTypes()
        {
            var list = new List<Type>();
            foreach (var assembly in GetAllAssemblies())
            {
                var typeInfos = assembly.DefinedTypes;
                foreach (var typeInfo in typeInfos)
                    list.Add(typeInfo.AsType());
            }
            return list;
        }
        /// <summary>
        /// 从程序集获取类型集
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static IList<Type> GetTypesByAssembly(string assemblyName)
        {
            var list = new List<Type>();
            var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(assemblyName));
            var typeInfos = assembly.DefinedTypes;
            foreach (var typeInfo in typeInfos)
                list.Add(typeInfo.AsType());
            return list;
        }
        /// <summary>
        /// 获取实现
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="baseInterfaceType"></param>
        /// <returns></returns>
        public static Type GetImplementType(string typeName, Type baseInterfaceType)
        {
            return GetAllTypes().FirstOrDefault(t =>
            {
                if (t.Name == typeName &&
                    t.GetTypeInfo().GetInterfaces().Any(b => b.Name == baseInterfaceType.Name))
                {
                    var typeInfo = t.GetTypeInfo();
                    return typeInfo.IsClass && !typeInfo.IsAbstract && !typeInfo.IsGenericType;
                }
                return false;
            });
        }
    }
}
