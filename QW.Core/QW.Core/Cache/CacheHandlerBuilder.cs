﻿using QW.Core.Helper;
using QW.Core.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache
{
    /// <summary>
    /// 缓存处理器构建器
    /// </summary>
    public class CacheHandlerBuilder
    {
        private ICacheHandler _handler;
        /// <summary>
        /// 默认过期时间(分钟)
        /// </summary>
        private int DefaultTimeOut = 20;
        /// <summary>
        /// 缓存前缀
        /// </summary>
        private string Prefix = "qwc_";
        /// <summary>
        /// 构建器
        /// </summary>
        /// <returns></returns>
        public static CacheHandlerBuilder Create()
        {
            return new CacheHandlerBuilder();
        }
        /// <summary>
        /// 注册处理程序
        /// </summary>
        /// <param name="handle"></param>
        public CacheHandlerBuilder RegisterHandler(ICacheHandler handle)
        {
            _handler = handle;
            return this;
        }
        /// <summary>
        /// 设置默认过期时间(分钟)
        /// </summary>
        /// <param name="time"></param>
        public CacheHandlerBuilder SetDefaultTimeOut(int time)
        {
            this.DefaultTimeOut = time;
            return this;
        }
        /// <summary>
        /// 设置缓存名称前缀
        /// </summary>
        /// <param name="prefix"></param>
        public CacheHandlerBuilder SetPrefix(string prefix)
        {
            this.Prefix = prefix;
            return this;
        }
        /// <summary>
        /// 容器中心构建
        /// </summary>
        public void Build()
        {
            _handler.SetDefaultTimeOut(DefaultTimeOut);
            _handler.SetPrefix(Prefix);
            _handler.Build();
            CacheHandler.Register(_handler);
        }
    }
}
