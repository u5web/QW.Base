﻿using QW.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache
{
    /// <summary>
    /// 基缓存处理器
    /// </summary>
    public abstract class BaseCacheHandler
    {
        /// <summary>
        /// 默认过期时间(分钟)
        /// <para>默认20分钟</para>
        /// </summary>
        public int DefaultTimeOut { get; private set; } = 20;
        /// <summary>
        /// 缓存前缀
        /// </summary>
        public string Prefix { get; private set; } 
        /// <summary>
        /// 设置默认过期时间(分钟)
        /// </summary>
        /// <param name="time"></param>
        public void SetDefaultTimeOut(int time)
        {
            DefaultTimeOut = time;
        }
        /// <summary>
        /// 设置缓存名称前缀
        /// </summary>
        /// <param name="prefix"></param>
        public void SetPrefix(string prefix)
        {
            Prefix = prefix;
        }
        /// <summary>
        /// 构建
        /// </summary>
        public virtual void Build()
        {

        }
        /// <summary>
        /// 获取最终key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected string GetFinallyKey(string key)
        {
            if (key == null) { throw new ArgumentNullException(nameof(key)); }
            string result = Prefix + key;
            return result;
        }
        /// <summary>
        /// 获取完整Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetFullKey(string key)
        {
            return GetFinallyKey(key);
        }
    }
}
