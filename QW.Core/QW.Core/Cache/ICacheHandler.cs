﻿using QW.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache
{
    /// <summary>
    /// 缓存处理器接口
    /// </summary>
    public interface ICacheHandler
    {
        /// <summary>
        /// 默认过期时间(分钟)
        /// </summary>
        int DefaultTimeOut { get; }
        /// <summary>
        /// 缓存前缀
        /// </summary>
        string Prefix { get; }
        /// <summary>
        /// 设置默认过期时间(分钟)
        /// </summary>
        /// <param name="time"></param>
        void SetDefaultTimeOut(int time);
        /// <summary>
        /// 设置缓存名称前缀
        /// </summary>
        /// <param name="prefix"></param>
        void SetPrefix(string prefix);
        /// <summary>
        /// 构建
        /// </summary>
        void Build();
        /// <summary>
        /// 获取完整Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string GetFullKey(string key);

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Exists(string key);

        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);

        /// <summary>
        /// 将指定键的对象添加到缓存中，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔,null使用默认缓存时间</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        void Set<T>(string key, T data, TimeSpan? span = null, bool isLongTerm = false);

        /// <summary>
        /// 获得指定键的缓存集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        List<T> GetList<T>(string key);

        /// <summary>
        /// 将缓存集合存入指定键，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔,null使用默认缓存时间</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        void SetList<T>(string key, IList<T> data, TimeSpan? span = null, bool isLongTerm = false);

        /// <summary>
        /// 从缓存中移除指定键的缓存值
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);

        #region 不是所有缓存方案都能提供这两个数据，故改为不用实现
        /*
        /// <summary>
        /// 缓存总数
        /// </summary>
        /// <returns></returns>
        //int GetCount();
        /// <summary>
        /// 获取所有的key
        /// <para>会影响性能</para>
        /// </summary>
        /// <returns></returns>
        //List<string> GetAllKeys();
        */
        #endregion
    }
}
