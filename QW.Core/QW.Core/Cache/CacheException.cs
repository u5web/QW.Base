﻿using QW.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Cache
{
    /// <summary>
    /// 缓存异常
    /// </summary>
    public class CacheException : QWBaseException
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public CacheException() { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>

        public CacheException(string message) : base(message) { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public CacheException(string message, Exception inner) : base(message, inner) { }
    }
}
