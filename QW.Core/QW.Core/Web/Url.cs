﻿using System;
using System.Web;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;

namespace QW.Core.Web
{
    /// <summary>
    /// 网页辅助
    /// </summary>
    public class Url
    {
        /// <summary>
        /// 用于对网址进行参数处理,只处理有这个参数的地址
        /// </summary>
        /// <param name="url">源站地址</param>
        /// <param name="fname">字段名</param>
        /// <param name="val">值</param>
        /// <param name="ignoreCase">忽略大小写</param>
        /// <returns>处理过的网址值</returns>
        public static string Replace(string url, string fname, string val, bool ignoreCase = true)
        {
            string tmpstr = "";
            if (ignoreCase)
            {
                tmpstr = Regex.Replace(url, @"([\?&])" + fname + "=[^&]*(&?)", "$1" + fname + "=" + val + "$2", RegexOptions.IgnoreCase);
            }
            else
            {
                tmpstr = Regex.Replace(url, @"([\?&])" + fname + "=[^&]*(&?)", "$1" + fname + "=" + val + "$2");
            }
            return tmpstr;
        }
        /// <summary>
        /// 删除网址中的参数
        /// </summary>
        /// <param name="url">源站地址</param>
        /// <param name="fname">字段名</param>
        /// <param name="ignoreCase">忽略大小写</param>
        /// <returns>处理过的网址值</returns>
        public static string DeleteParam(string url, string fname, bool ignoreCase = true)
        {
            string tmpstr = "";
            if (ignoreCase)
            {
                tmpstr = Regex.Replace(url, @"([\?&])" + fname + "=[^&]*(&?)", "$1", RegexOptions.IgnoreCase);
            }
            else
            {
                tmpstr = Regex.Replace(url, @"([\?&])" + fname + "=[^&]*(&?)", "$1");
            }
            tmpstr = Regex.Replace(tmpstr, @"&$", "");
            return tmpstr;
        }
        /// <summary>
        /// 用于对网址进行参数处理,没有就添加这个参数
        /// </summary>
        /// <param name="url">源站地址</param>
        /// <param name="fname">字段名</param>
        /// <param name="val">值</param>
        /// <param name="ignoreCase">忽略大小写</param>
        /// <returns>处理过的网址值</returns>
        public static string AddOrReplace(string url, string fname, string val, bool ignoreCase = true)
        {
            string tmpstr = "";
            string regstr = @"([\?&])" + fname + "=[^&]*(&?)";
            if (ignoreCase)
            {
                tmpstr = Regex.Replace(url, regstr, "$1", RegexOptions.IgnoreCase);
            }
            else
            {
                tmpstr = Regex.Replace(url, regstr, "$1");
            }
            if ("?&".IndexOf(tmpstr.Substring(tmpstr.Length - 1)) > -1) { tmpstr = tmpstr.Substring(0, tmpstr.Length - 1); }
            if (tmpstr.IndexOf("?") > -1)
            {
                tmpstr += "&";
            }
            else
            {
                tmpstr += "?";
            }
            tmpstr += fname + "=" + val;
            return tmpstr;
        }
        /// <summary>
        /// 取得当前页的查询参数
        /// </summary>
        /// <returns>不带?开头的参数</returns>
        public static string GetQueryParams()
        {
            StringBuilder stringBuilder = new StringBuilder();
            string currentPath = HttpContext.GetAbsoluteUri(HttpContext.Current.Request);
            int startIndex = currentPath.IndexOf("?");
            if (startIndex <= 0) return string.Empty;
            string[] nameValues = currentPath.Substring(startIndex + 1).Split('&');
            foreach (string param in nameValues)
            {
                stringBuilder.Append(param);
                stringBuilder.Append("&");
            }
            return stringBuilder.ToString().TrimEnd('&');
        }
        /// <summary>
        /// 获取基础url
        /// </summary>
        /// <returns></returns>
        public static string GetBaseUrl()
        {
            return HttpContext.GetBaseUri(HttpContext.Current.Request);
        }
    }
}