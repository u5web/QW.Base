﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Exceptions
{
    /// <summary>
    /// 基础业务异常类
    /// </summary>
    public class QWBaseBusinessException : QWBaseException
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public QWBaseBusinessException() { }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        public QWBaseBusinessException(string message) : base(message) { }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public QWBaseBusinessException(string message, Exception inner) : base(message, inner) { }
    }
}
