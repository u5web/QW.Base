﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.Exceptions
{
    /// <summary>
    /// 创建实例异常
    /// </summary>
    public class InstanceCreateException : QWBaseException
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public InstanceCreateException() { }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        public InstanceCreateException(string message) : base(message) { }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public InstanceCreateException(string message, Exception inner) : base(message, inner) { }
    }
}
