﻿using QW.Core.Helper;
using QW.Core.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.File
{
    /// <summary>
    /// 文件处理器构建器
    /// </summary>
    public class FileHandlerBuilder
    {
        private IFileHandler _handler;
        /// <summary>
        /// 构建器
        /// </summary>
        /// <returns></returns>
        public static FileHandlerBuilder Create()
        {
            return new FileHandlerBuilder();
        }
        /// <summary>
        /// 注册处理程序
        /// </summary>
        /// <param name="handle"></param>
        public FileHandlerBuilder RegisterHandler(IFileHandler handle)
        {
            _handler = handle;
            return this;
        }
        /// <summary>
        /// 容器中心构建
        /// </summary>
        public void Build()
        {
            _handler.Build();
            FileHandler.Register(_handler);
        }
    }
}
