﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.File
{
    /// <summary>
    /// 文件元信息
    /// </summary>
    public class FileMetaInfo
    {
        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime LastModifiedTime { get; set; }
        /// <summary>
        /// 最后访问时间
        /// </summary>
        public DateTime LastAccessTime { get; set; }
        /// <summary>
        /// 内容长度
        /// </summary>
        public long ContentLength { get; set; }
        /// <summary>
        /// 扩展名
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 属性
        /// </summary>
        public FileAttributes Attributes { get; set; }
    }
}
