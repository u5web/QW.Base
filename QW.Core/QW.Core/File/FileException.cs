﻿using QW.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Core.File
{
    /// <summary>
    /// IO异常
    /// </summary>
    public class FileException : QWBaseException
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// .ctor
        /// </summary>
        public FileException() { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>

        public FileException(string message) : base(message) { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName"></param>

        public FileException(string message, string fileName) : base(message)
        {
            this.FileName = fileName;
        }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>

        public FileException(string message, Exception inner) : base(message, inner) { }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName"></param>
        /// <param name="inner"></param>

        public FileException(string message, string fileName, Exception inner) : base(message, inner)
        {
            this.FileName = fileName;
        }
    }
}
