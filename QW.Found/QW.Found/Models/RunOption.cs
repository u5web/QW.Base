﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.Models
{
    /// <summary>
    /// 运行选项
    /// </summary>
    public class RunOption
    {
        /// <summary>
        /// 运行方式
        /// <para>Service 服务方式,Run 运行方式</para>
        /// </summary>
        public string RunType
        {
            get; set;
        }
        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName
        {
            get; set;
        }
        /// <summary>
        /// 服务描述
        /// </summary>        
        public string ServiceDescription
        {
            get; set;
        }
    }
}
