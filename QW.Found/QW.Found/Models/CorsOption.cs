﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.Models
{
    /// <summary>
    /// Cors跨域选项
    /// </summary>
    public class CorsOption
    {
        /// <summary>
        /// 策略名称
        /// <para>为空表示不开启</para>
        /// </summary>
        public string PolicyName
        {
            get; set;
        }
        /// <summary>
        /// 起源
        /// </summary>
        public string Origins
        {
            get; set;
        }
    }
}
