﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.Models
{
    /// <summary>
    /// swagger选项
    /// </summary>
    public class SwaggerOption
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get; set;
        }
        /// <summary>
        /// 版本
        /// </summary>
        public string Version
        {
            get; set;
        }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get; set;
        }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            get; set;
        }
    }
}
