﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QW.Core.Web;

namespace QW.Found.Helper
{
    /// <summary>
    /// 验证码
    /// </summary>
    /// 
    public class CaptchaHelper
    {
        /// <summary>
        /// 构造
        /// </summary>
        public CaptchaHelper() { }

        private const string CAPTCHA_CACHE_NAME = "sys_captcha";

        /// <summary>
        /// 随机对象
        /// </summary>
        private static readonly Random g_random = new Random();

        /// <summary>
        /// 生成验证码并存入session
        /// </summary>
        /// <param name="minChars"></param>
        /// <param name="maxChars"></param>
        /// <returns></returns>
        public static string MakeCaptcha(int minChars, int maxChars)
        {
            string value = "";
            Random g_random = new Random();
            // 随机产生字符串长度
            int strLen = g_random.Next(minChars, maxChars);
            // 计数器
            int count = 0;

            while (count < strLen)
            {
                // 产生随机字符 0..9, a..z, A..Z
                int n = g_random.Next(48, 121);

                // 如果不是数字或者字母, 则跳过
                if ((n >= 58 && n <= 64) || (n >= 91 && n <= 96))
                    continue;

                // 不为 I, O, i,o
                if (n == 'I' || n == 'i' ||
                    n == 'o' || n == 'O')
                    continue;

                value += Convert.ToChar(n);

                count++;
            }
            SetCaptcha(value);
            return value;
        }
        /// <summary>
        /// 保存验证码到session
        /// </summary>
        /// <param name="code"></param>
        public static void SetCaptcha(string code)
        {
            var hc = HttpContext.Current;
            byte[] _v = System.Text.Encoding.UTF8.GetBytes(code);
            hc.Session.Set(CAPTCHA_CACHE_NAME, _v);
        }
        /// <summary>
        /// 获取当前验证码
        /// </summary>
        /// <returns></returns>
        public static string GetCaptcha()
        {
            var hc = HttpContext.Current;
            byte[] _v;
            var _ses = hc.Session.TryGetValue(CAPTCHA_CACHE_NAME, out _v);
            string result = "";
            if (_ses)
            {
                result = System.Text.Encoding.UTF8.GetString(_v);
            }
            return result;
        }
        /// <summary>
        /// 验证验证码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool Validate(string code)
        {
            bool result = false;
            var syscode = GetCaptcha();
            if (!string.IsNullOrWhiteSpace(syscode) && !string.IsNullOrWhiteSpace(code))
            {
                syscode = syscode.ToLower();
                code = code.ToLower();
                if (syscode == code)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
