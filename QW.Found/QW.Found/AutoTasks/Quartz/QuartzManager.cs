﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.AutoTasks.Quartz
{
    /// <summary>
    /// Quartz管理器
    /// </summary>
    public class QuartzManager
    {
        //调度器
        protected readonly IScheduler scheduler;
        /// <summary>
        /// .ctor
        /// </summary>
        public QuartzManager()
        {
            //this.scheduler = center.scheduler;
        }
        /// <summary>
        /// 获取正在运行的Jobs
        /// </summary>
        /// <returns></returns>
        public async Task<object> GetCurrentlyExecutingJobs()
        {
            var jobs = await scheduler.GetCurrentlyExecutingJobs();
            /*
            foreach (var job in jobs)
            {

            }
            */
            return jobs;
        }
    }
}
