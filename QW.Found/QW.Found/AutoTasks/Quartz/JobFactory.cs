﻿using System;
using System.Collections.Generic;
using System.Text;
using Quartz;
using Quartz.Spi;

namespace QW.Found.TasksTasks.Quartz
{
    /// <summary>
    /// Job工厂
    /// </summary>
    public class JobFactory : IJobFactory
    {
        private readonly IServiceProvider provider;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="provider"></param>
        public JobFactory(IServiceProvider provider)
        {
            this.provider = provider;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var job = provider.GetService(typeof(IJob));
            return (IJob)job;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }
    }
}
