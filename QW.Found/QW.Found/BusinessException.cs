﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found
{
    /// <summary>
    /// 业务异常
    /// <para>主动抛出异常</para>
    /// </summary>
    public class BusinessException : Exception
    {
        /// <summary>
        /// 错误编号
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 业务异常
        /// </summary>
        public BusinessException()
        {
        }
        /// <summary>
        /// 业务异常
        /// </summary>
        public BusinessException(string message) : base(message) { }

        /// <summary>
        /// 业务异常
        /// </summary>
        public BusinessException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// 业务异常
        /// </summary>
        public BusinessException(string message, int code) : base(message)
        {
            Code = code;
        }

        /// <summary>
        /// 业务异常
        /// </summary>
        public BusinessException(string message, int code, Exception inner) : base(message, inner)
        {
            Code = code;
        }
    }
}
