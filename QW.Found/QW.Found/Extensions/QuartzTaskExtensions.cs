﻿using Microsoft.Extensions.DependencyInjection;
using Quartz.Impl;
using Quartz.Spi;
using Quartz;
using QW.Core.AutoTasks;
using QW.Found.AutoTasks.Quartz;
using QW.Found.TasksTasks.Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.Extensions
{
    /// <summary>
    /// Quartz任务扩展
    /// </summary>
    public static class QuartzTaskExtensions
    {
        /// <summary>
        /// 注册 Quartz 任务框架
        /// </summary>
        /// <param name="services"></param>
        /// <param name="projectTypes"></param>
        public static void AddQuartzTaskCenter(this IServiceCollection services, List<Type> projectTypes)
        {
            services.AddTaskCenter(projectTypes);

            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<IJobFactory, JobFactory>();
            services.AddTransient<IJob, TaskJob>();
            services.AddSingleton<ITaskCenter, QuartzTaskCenter>();
            services.AddSingleton<QuartzManager>();
        }
    }
}
