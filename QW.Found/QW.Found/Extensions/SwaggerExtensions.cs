﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using QW.Found.Filter;
using Microsoft.OpenApi.Models;
using QW.Found.JwtAuthorize;
using QW.Found.Models;

namespace EIP.Infrastructure.Extensions
{
    /// <summary>
    /// Swagger注入
    /// </summary>
    public static class SwaggerExtensions
    {
        /// <summary>
        /// 注入Swagger
        /// </summary>
        /// <param name="services"></param>
        public static void AddMySwagger(this IServiceCollection services, List<SwaggerOption> swaggers)
        {
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            //.net 6才有
            //services.AddEndpointsApiExplorer();

            services.AddSwaggerGen(c =>
            {
                if (swaggers != null && swaggers.Count > 0)
                {
                    foreach (var s in swaggers)
                    {
                        var info = new OpenApiInfo
                        {
                            Version = s.Version,
                            Title = s.Title,
                            Description = s.Description
                        };
                        c.SwaggerDoc(s.Name, info);
                    }
                }
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                foreach (var path in global::System.IO.Directory.GetFiles(basePath, "*.xml"))
                {
                    c.IncludeXmlComments(path, true);
                }
                c.SchemaFilter<SwaggerSchemaFilter>();
                //添加jwt授权验证
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "请输入带有Bearer的Token", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme{
                Reference=new OpenApiReference() { Id=JWTConfig.DEFAULT_SCHEME, Type=ReferenceType.SecurityScheme }
            },new string[] { }
        }
    });

            });
        }
        /// <summary>
        /// 使用Swagger
        /// </summary>
        /// <param name="app"></param>
        public static void UseMySwagger(this IApplicationBuilder app, List<SwaggerOption> swaggers)
        {
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                if (swaggers != null && swaggers.Count > 0)
                {
                    foreach (var s in swaggers)
                    {
                        options.SwaggerEndpoint("/swagger/" + s.Name + "/swagger.json", s.Description);
                    }
                }
            });
        }
    }
}
