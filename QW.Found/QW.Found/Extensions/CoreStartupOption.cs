﻿using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using QW.Found.JwtAuthorize;
using QW.Found.Models;

namespace QW.Found.Extensions
{
    /// <summary>
    /// 内核启动选项
    /// </summary>
    public class CoreStartupOption
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public CoreStartupOption()
        {
            ProjectAssemblies = new List<Assembly>();
            ProjectTypes = new List<Type>();
        }
        /// <summary>
        /// 项目程序集合
        /// </summary>
        public List<Assembly> ProjectAssemblies { get; set; }
        /// <summary>
        /// 项目类型集合
        /// </summary>
        public List<Type> ProjectTypes { get; set; }
        /// <summary>
        /// jwt配置
        /// </summary>
        public JWTConfig JwtConfig { get; set; }
        /// <summary>
        /// 跨域选项
        /// </summary>
        public CorsOption CorsOption { get; set; }
        /// <summary>
        /// 权限二次验证
        /// </summary>
        public Func<AuthorizationHandlerContext, bool> PermissionAction { get; set; }
    }
}
