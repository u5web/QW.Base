﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace QW.Found.JwtAuthorize
{
    /// <summary>
    /// 对Token先AES解密再验证
    /// </summary>
    public class AESTokenValidator : ISecurityTokenValidator
    {
        /// <summary>
        /// 
        /// </summary>
        public int MaximumTokenSizeInBytes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public bool CanReadToken(string securityToken)
        {
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        public bool CanValidateToken
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 验证token
        /// </summary>
        /// <param name="securityToken"></param>
        /// <param name="validationParameters"></param>
        /// <param name="validatedToken"></param>
        /// <returns></returns>
        public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            string jwtToken = securityToken;

            // 先解密token
            // jwtToken= AESCryptoHelper.Decrypt(jwtToken);

            ClaimsPrincipal principal = new JwtSecurityTokenHandler().ValidateToken(jwtToken, validationParameters, out validatedToken);
            return principal;
        }
    }
}
