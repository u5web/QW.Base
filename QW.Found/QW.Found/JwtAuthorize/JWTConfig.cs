﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.JwtAuthorize
{
    /// <summary>
    /// Jwt配置
    /// </summary>
    public class JWTConfig
    {
        /// <summary>
        /// 策略名称
        /// </summary>
        public const string POLICY_NAME = "permission";
        /// <summary>
        /// 默认架构
        /// </summary>
        public const string DEFAULT_SCHEME= "Bearer";
        /// <summary>
        /// 密钥
        /// </summary>
        public string Secret { get; set; }
        /// <summary>
        /// 颁发者
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// 订阅
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 是否仅接受https验证
        /// </summary>
        public bool IsHttps { get; set; }
        /// <summary>
        /// 令牌是否必须有过期时间
        /// <para>强制要求必须有</para>
        /// </summary>
        //public bool RequireExpirationTime { get; private set; } = true;
    }
}
