﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.JwtAuthorize
{
    /// <summary>
    /// 令牌
    /// </summary>
    public class TokenInfo
    {
        /// <summary>
        /// 值
        /// </summary>
        public string TokenValue { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? Expires { get; set; }
        /// <summary>
        /// 架构
        /// </summary>
        public string Scheme { get; set; }
    }
}
