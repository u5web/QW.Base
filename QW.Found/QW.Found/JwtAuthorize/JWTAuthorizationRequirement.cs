﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace QW.Found.JwtAuthorize
{

    /// <summary>
    /// jwt授权要求
    /// </summary>
    public class JwtAuthorizationRequirement : IAuthorizationRequirement
    {
        /// <summary>
        /// 颁发者
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// 订阅
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 架构
        /// </summary>
        public string Scheme { get; set; }
        /// <summary>
        /// 数字签名证书
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }

        /// <summary>
        /// 权限验证
        /// </summary>
        public Func<AuthorizationHandlerContext, bool> ValidatePermission { get; internal set; }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="issuer">颁发者</param>
        /// <param name="audience">订阅</param>
        /// <param name="scheme">架构</param>
        /// <param name="signingCredentials">数字签名证书</param>
        public JwtAuthorizationRequirement(string issuer, string audience, string scheme, SigningCredentials signingCredentials)
        {
            Issuer = issuer;
            Audience = audience;
            Scheme = scheme;
            SigningCredentials = signingCredentials;
        }
    }
}
