﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using static QW.Found.JwtAuthorize.TokenBuilder;

namespace QW.Found.JwtAuthorize
{
    /// <summary>
    /// 令牌构建器接口
    /// </summary>
    public interface ITokenBuilder
    {
        /// <summary>
        /// 构建令牌
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="requirement"></param>
        /// <param name="expires"></param>
        /// <param name="notBefore">此时间后才会生效</param>
        /// <returns></returns>
        TokenInfo BuildJwtToken(IEnumerable<Claim> claims, DateTime expires, DateTime? notBefore = null);
    }
}
