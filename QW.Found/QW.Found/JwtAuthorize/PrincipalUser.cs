﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QW.Found.JwtAuthorize
{
    /// <summary>
    /// 登录用户基础信息
    /// </summary>
    public class PrincipalUser<T>
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 登录编号
        /// </summary>
        public string LoginId { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// 显示姓名
        /// </summary>
        public string ShowName { get; set; }
        /// <summary>
        /// Ip
        /// </summary>
        public string RemoteIp { get; set; }
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LoginTime { get; set; }
        /// <summary>
        /// 扩展参数
        /// </summary>
        public T External { get; set; }
    }
}
