﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace QW.Found.Filter
{
    public class ActionResultFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 动作结果过滤器
        /// </summary>
        private readonly IHttpContextAccessor _accessor;
        public ActionResultFilter(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        /// <summary>
        /// 执行完毕
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }

        /// <summary>
        /// 开始执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            /*
            var request = context.HttpContext.Request;
            var ControllerName = ((ControllerActionDescriptor)context.ActionDescriptor).ControllerName;
            var ActionName = ((ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            //处理参数
            var actionParameters = context.ActionArguments;
            if (actionParameters != null)
            {
                foreach (var p in actionParameters)
                {
                    var obj = p.Value;
                    if (obj != null && !(obj is string))
                    {
                        var type = obj.GetType();
                        var propertys = obj.GetType().GetProperties();
                        foreach (var item in propertys)
                        {
                            var name = item.Name;
                            var val = item.GetValue(obj, null);
                        }
                    }
                }
            }
            var descriptionAttribute = context.ActionDescriptor.FilterDescriptors;
            if(!descriptionAttribute.Any())
                return;
            */
        }

        /// <summary>
        /// 结果返回开始
        /// </summary>
        /// <param name="context"></param>
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            base.OnResultExecuting(context);
        }

        /// <summary>
        /// 结果返回完毕
        /// </summary>
        /// <param name="context"></param>
        public override void OnResultExecuted(ResultExecutedContext context)
        {
            base.OnResultExecuted(context);
            /*
            var response = context.HttpContext.Response;
            var ControllerName = ((ControllerActionDescriptor)context.ActionDescriptor).ControllerName;
            var ActionName = ((ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            */
        }
    }
}