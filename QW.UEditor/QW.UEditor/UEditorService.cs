﻿//***********************
//* 感谢 https://github.com/durow/UEditorNetCore
//***********************
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QW.UEditor.Handlers;
using Microsoft.AspNetCore.Hosting;

namespace QW.UEditor
{
    /// <summary>
    /// 
    /// </summary>
    public class UEditorService
    {
        private ActionCollection actionList;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="actions"></param>
        public UEditorService(ActionCollection actions)
        {
            actionList = actions;
        }
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="context"></param>
        public void DoAction(HttpContext context)
        {
            var action = context.Request.Query["action"];
            if (actionList.ContainsKey(action))
                actionList[action].Invoke(context);
            else
                new NotSupportedHandler(context).Process();
        }
    }
}
