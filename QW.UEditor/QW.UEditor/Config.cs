﻿using Newtonsoft.Json.Linq;
using System;
using QW.Core.File;
using System.Buffers.Text;
using System.Text;
using System.Linq;
using System.IO;
using QW.Core.Helper;
using QW.Core.File.Local;

namespace QW.UEditor
{
    /// <summary>
    /// 配置信息
    /// </summary>
    public class Config
    {
        /// <summary>
        /// 保存文件的根目录
        /// </summary>
        public static string SaveFileRootPath { get; set; }
        /// <summary>
        /// 配置文件地址
        /// </summary>
        public static string ConfigFile { set; get; }
        /// <summary>
        /// 配置文件是本地文件
        /// <para>仅对配置文件有效，上传文件的读写都是通过FileHandlerBuilder管理</para>
        /// <para>false(默认):表示接受FileHandlerBuilder管理.true:表示强制使用本地方式读取配置文件</para>
        /// </summary>
        public static bool IsLocalConfig { get; set; } = false;
        /// <summary>
        /// 
        /// </summary>
        private static JObject _Items { get; set; }
        private static JObject BuildItems()
        {
            if (string.IsNullOrWhiteSpace(ConfigFile))
            {
                throw new IOException("请配置ue配置文件地址");
            }
            string json = "";
            if (IsLocalConfig)
            {
                var _fh = new LocalFileHandler();
                json = Encoding.UTF8.GetString(_fh.GetFileContent(ConfigFile));
            }
            else
            {
                json = Encoding.UTF8.GetString(FileHandler.GetFileContent(ConfigFile));
            }
            return JObject.Parse(json);
        }
        /// <summary>
        /// 项
        /// </summary>
        public static JObject Items
        {
            get
            {
                if (_Items == null)
                {
                    _Items = BuildItems();
                }
                return _Items;
            }
        }
        /// <summary>
        /// 刷新
        /// </summary>
        public static void RefreshItems()
        {
            _Items = null;
        }
        /// <summary>
        /// 获取项值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            return Items[key].Value<T>();
        }
        /// <summary>
        /// 获取字符串数组项值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static String[] GetStringList(string key)
        {
            return Items[key].Select(x => x.Value<String>()).ToArray();
        }
        /// <summary>
        /// 获取字符串项值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static String GetString(string key)
        {
            return Get<String>(key);
        }
        /// <summary>
        /// 获取整形项值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetInt(string key)
        {
            return Get<int>(key);
        }
    }
}
