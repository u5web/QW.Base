using Microsoft.AspNetCore.Http;
using QW.Core.File;
using QW.Core.Helper;
using System;
using System.IO;
using System.Linq;
using System.Net;

namespace QW.UEditor.Handlers
{
    /// <summary>
    /// 涂鸦
    /// </summary>
    public class CrawlerHandler : BaseHandler
    {
        private string[] Sources;
        private Crawler[] Crawlers;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="context"></param>
        public CrawlerHandler(HttpContext context) : base(context) { }
        /// <summary>
        /// 处理过程
        /// </summary>
        public override void Process()
        {
            Sources = Request.Form["source[]"];
            if (Sources == null || Sources.Length == 0)
            {
                WriteJson(new
                {
                    state = "参数错误：没有指定抓取源"
                });
                return;
            }
            Crawlers = Sources.Select(x => new Crawler(x).Fetch()).ToArray();
            WriteJson(new
            {
                state = "SUCCESS",
                list = Crawlers.Select(x => new
                {
                    state = x.State,
                    source = x.SourceUrl,
                    url = x.ServerUrl
                })
            });
        }
    }
    /// <summary>
    /// 涂鸦
    /// </summary>
    public class Crawler
    {
        /// <summary>
        /// 源
        /// </summary>
        public string SourceUrl { get; set; }
        /// <summary>
        /// 目标
        /// </summary>
        public string ServerUrl { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string State { get; set; }

        // private HttpServerUtility Server { get; set; }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="sourceUrl"></param>
        public Crawler(string sourceUrl)
        {
            this.SourceUrl = sourceUrl;
            // this.Server = server;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Crawler Fetch()
        {
            if (!IsExternalIPAddress(this.SourceUrl))
            {
                State = "INVALID_URL";
                return this;
            }
            var request = HttpWebRequest.Create(this.SourceUrl) as HttpWebRequest;
            using (var response = request.GetResponseAsync().Result as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    State = "Url returns " + response.StatusCode + ", " + response.StatusDescription;
                    return this;
                }
                if (response.ContentType.IndexOf("image") == -1)
                {
                    State = "Url is not an image";
                    return this;
                }
                var savePath = PathFormatter.Format(Path.GetFileName(this.SourceUrl), Config.GetString("catcherPathFormat"));
                var localPath = IOHelper.PathCombine(Config.SaveFileRootPath, savePath);

                try
                {
                    var stream = response.GetResponseStream();
                    if (!FileHandler.ExistFolder(Path.GetDirectoryName(localPath)))
                    {
                        FileHandler.CreateFolder(Path.GetDirectoryName(localPath));
                    }

                    var reader = new BinaryReader(stream);
                    byte[] bytes;
                    using (var ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[4096];
                        int count;
                        while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            ms.Write(buffer, 0, count);
                        }

                        bytes = ms.ToArray();
                    }
                    FileHandler.CreateFile(localPath, bytes);
                    this.ServerUrl = savePath;
                    State = "SUCCESS";
                }
                catch (Exception e)
                {
                    State = "抓取错误：" + e.Message;
                }
                return this;
            }
        }

        private bool IsExternalIPAddress(string url)
        {
            var uri = new Uri(url);
            switch (uri.HostNameType)
            {
                case UriHostNameType.Dns:
                    var ipHostEntry = Dns.GetHostEntryAsync(uri.DnsSafeHost).Result;
                    foreach (IPAddress ipAddress in ipHostEntry.AddressList)
                    {
                        byte[] ipBytes = ipAddress.GetAddressBytes();
                        if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            if (!IsPrivateIP(ipAddress))
                            {
                                return true;
                            }
                        }
                    }
                    break;

                case UriHostNameType.IPv4:
                    return !IsPrivateIP(IPAddress.Parse(uri.DnsSafeHost));
            }
            return false;
        }

        private bool IsPrivateIP(IPAddress myIPAddress)
        {
            if (IPAddress.IsLoopback(myIPAddress)) return true;
            if (myIPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                byte[] ipBytes = myIPAddress.GetAddressBytes();
                // 10.0.0.0/24 
                if (ipBytes[0] == 10)
                {
                    return true;
                }
                // 172.16.0.0/16
                else if (ipBytes[0] == 172 && ipBytes[1] == 16)
                {
                    return true;
                }
                // 192.168.0.0/16
                else if (ipBytes[0] == 192 && ipBytes[1] == 168)
                {
                    return true;
                }
                // 169.254.0.0/16
                else if (ipBytes[0] == 169 && ipBytes[1] == 254)
                {
                    return true;
                }
            }
            return false;
        }
    }
}