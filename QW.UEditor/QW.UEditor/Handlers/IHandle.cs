﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QW.UEditor.Handlers
{
    /// <summary>
    /// 处理器接口
    /// </summary>
    public interface IHandle
    {
        /// <summary>
        /// 处理过程
        /// </summary>
        /// <returns></returns>
        void Process();
    }
}
