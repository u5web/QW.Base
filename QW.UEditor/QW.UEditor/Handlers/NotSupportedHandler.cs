using Microsoft.AspNetCore.Http;

namespace QW.UEditor.Handlers
{
    /// <summary>
    /// 不支持处理器
    /// </summary>
    public class NotSupportedHandler : BaseHandler
    {
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="context"></param>
        public NotSupportedHandler(HttpContext context)
            : base(context)
        {
        }
        /// <summary>
        /// 处理过程
        /// </summary>
        public override void Process()
        {
            WriteJson(new
            {
                state = "action is empty or action not supperted."
            });
        }
    }
}