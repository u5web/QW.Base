using Microsoft.AspNetCore.Http;

namespace QW.UEditor.Handlers
{
    /// <summary>
    /// 配置处理器
    /// </summary>
    public class ConfigHandler : BaseHandler
    {
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="context"></param>
        public ConfigHandler(HttpContext context) : base(context) { }
        /// <summary>
        /// 处理过程
        /// </summary>
        public override void Process()
        {
            WriteJson(Config.Items);
        }
    }
}