﻿# QW.UEditor

#### 介绍
* NetCore下使用UEditor的一个封装
* 参考并特别感谢：https://github.com/durow/UEditorNetCore
* UEditor官网：http://ueditor.baidu.com/website/index.html
* UEditor源代码：https://github.com/fex-team/ueditor

#### 支持框架
netstandard2.0; 建议netcore 3.1,不支持NetFX

#### 第三方依赖
QW.Core 1.0.0,Newtonsoft.Json 12.0.2

#### 使用说明
1. 直接生dll后引用
2. 在Startup.cs的ConfigureServices方法中添加
```csharp
services.AddUEditor();
```
3. 在Startup.cs的Configure方法中添加
```csharp
app.UseUEditor(env.WebRootPath);
```
4. 不使用中间件方式可以选择添加Controller用于处理来自UEditor的请求
```csharp
public class UEditorController : Controller
{
    private UEditorService ue;
    public UEditorController(UEditorService ue){ this.ue = ue; }
    public void Do(){ ue.DoAction(HttpContext); }
}
```
5.修改前端配置文件ueditor.config.js
```javascript
serverUrl:"/UEditor"
```
6.扩展修改action
```csharp
app.UseUEditor(env.WebRootPath)
   .Add("test", context =>
        {
            context.Response.WriteAsync("test");
        })
```
7.移除action
```csharp
app.UseUEditor(env.WebRootPath)
   .Remove("uploadscrawl");
```

#### UseUEditor参数说明
|参数   |说明   |
| :------------ | :------------ |
|saveFileRootPath   |保存文件的根目录地址   |
|configFile   |配置文件地址   |
|isLocalConfig   |配置文件是否是存在本地   |
|isUseMiddleware   |是否使用中间件方式   |
|mapName   |中间件映射名,默认/UEditor   |

#### 自带action
|参数   |说明   |
| :------------ | :------------ |
|config   |配置信息   |
|uploadimage   |上传图片   |
|uploadscrawl   |上传涂鸦   |
|uploadvideo   |上传视频   |
|uploadfile   |上传文件   |
|listimage   |图片列表   |
|listfile   |文件列表   |
|catchimage   |下载远程图片   |
