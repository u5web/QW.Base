﻿using QW.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Reflection;
using System.Collections;
using QW.Core.Cache;

namespace QW.Cache.Redis
{
    /// <summary>
    /// Redis缓存处理器
    /// </summary>
    public class RedisCacheHandler : BaseCacheHandler, ICacheHandler
    {
        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            key = GetFinallyKey(key);
            T result = default(T);
            result = RedisHelper.Get<T>(key);
            return result;
        }

        /// <summary>
        /// 将指定键的对象添加到缓存中，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        public void Set<T>(string key, T data, TimeSpan? span = null, bool isLongTerm = false)
        {
            key = GetFinallyKey(key);
            var _ct = DateTimeOffset.Now.AddMinutes(DefaultTimeOut);
            if (span.HasValue)
            {
                _ct = DateTimeOffset.Now.Add(span.Value);
            }
            var sec = (int)(_ct - DateTime.Now).TotalSeconds;
            if (!isLongTerm)
            {
                RedisHelper.Set(key, data, sec);
            }
            else
            {
                RedisHelper.Set(key, data, -1);
            }
        }
        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<T> GetList<T>(string key)
        {
            key = GetFinallyKey(key);
            List<T> result = default(List<T>);
            var datas = RedisHelper.LRange<T>(key, 0, -1);
            if (datas != null)
            {
                result = datas.ToList();
            }
            return result;
        }

        /// <summary>
        /// 将缓存集合存入指定键，并指定过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="span">缓存过期时间间隔</param>
        /// <param name="isLongTerm">是否长期缓存，长期缓存会不受过期时间影响</param>
        public void SetList<T>(string key, IList<T> data, TimeSpan? span = null, bool isLongTerm = false)
        {
            key = GetFinallyKey(key);
            var _ct = DateTimeOffset.Now.AddMinutes(DefaultTimeOut);
            if (span.HasValue)
            {
                _ct = DateTimeOffset.Now.Add(span.Value);
            }
            var sec = (int)(_ct - DateTime.Now).TotalSeconds;
            var rdata = data.ToArray();  //不执行此步的话会把list整体存入，而不是按list存入
            RedisHelper.RPush(key, rdata);
            if (!isLongTerm)
            {
                RedisHelper.Expire(key, sec);
            }
        }
        /// <summary>
        /// 从缓存中移除指定键的缓存值
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            key = GetFinallyKey(key);
            if (RedisHelper.Exists(key))
            {
                RedisHelper.Del(key);
            }
        }
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            bool result = false;
            key = GetFinallyKey(key);
            result = RedisHelper.Exists(key);
            return result;
        }
    }
}
